# Dashboard

## Initialisation

### Prerequisites

- Download node
- Install PHP (e.g. XAMPP) locally

### Clone this repository

```
git clone https://git.infitech.in/nec/necdashboard-client.git
```

Copy paste the files to php root location. For example, in Windows: `C:\xampp\htdocs\`

### Running the application

- Install the dependencies by running

```
npm install
```

- Run the application using

```
npm run start
```

- This will run the dashboard in `http://localhost:4200`

## Code structure

### API

All API files are located in `./src/api/`. API is written in PHP which connects to MSSQL and MySQL. MSSQL is for all NEC related data. MySQL will be for all dashboard related data.

### Front end

The front-end is created using Angular 8. The application is divided into:

- `core`: where all the core dependencies sit i.e. dependencies used by all/nearly all modules
- `guards`: for root related guards
- `modules`: all page related modules go here
- `shared`: all shared code across modules

## Building the app

Run the following command

```
npm run build
```

This will create folder in `dist` with all the built javascript files.
