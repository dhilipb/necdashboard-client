<?php

class DatabaseClient {
    private $msSqlConnection = null;

    function __construct($database) {
        try {
            $this->msSqlConnection = new PDO("sqlsrv:Server=".MSSQL_HOST.";Database=".$database, MSSQL_USER, MSSQL_PASS);
            $this->msSqlConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

    function __destruct() {
        if ($this->msSqlConnection !== null) {
            $this->msSqlConnection = null;
        }
    }

    function fetch($sql, $cond = [], $resultType = PDO::FETCH_ASSOC) {
        $sqlStatement = $this->query($sql, $cond);
        $data = $sqlStatement->fetchAll($resultType);
        return $data;
    }

    function fetchFirst($sql, $cond = [], $resultType = PDO::FETCH_ASSOC) {
        $data = $this->fetch($sql, $cond, $resultType);
        if (count($data) > 0) {
            return $data[0];
        }
        return null;
    }
    
    function query($sql, $cond = []) {
        $sqlStatement = $this->msSqlConnection->prepare($sql);
        try {
            $sqlStatement->execute($cond);
        } catch (PDOException $e) {
            throw new Exception($e->getMessage() . " " . $sql);
        }
        return $sqlStatement;
    }

    function contains($sql, $cond = [], $resultType = PDO::FETCH_ASSOC) {
        $data = $this->fetch($sql, $cond, $resultType);
        return count($data) > 0;
    }

    function insert($table, $colValues) { // colValues = {username: dhilip, password: dhilip}
        $columns = '[' . implode('], [', array_keys($colValues)) . ']'; // username, password
        $valueVars = ':' . implode(', :', array_keys($colValues)); // :username, :password
        $sql = "INSERT INTO $table($columns) VALUES($valueVars)"; // INSERT INTO db_login(username, password) VALUES(:username, :password)
        $this->query($sql, $colValues); 
        $id = $this->msSqlConnection->lastInsertId();
        return $id;
    }

    function update($table, $colValues, $whereCond, $whereColValues) {
        $setSql = [];
        foreach ($colValues as $col => $val) {
            array_push($setSql, "[$col] = :$col");
        }
        $setSql = implode(', ', $setSql);
        $setSql = "$setSql, modified = GETDATE()";

        $sql = "UPDATE $table SET $setSql WHERE $whereCond";
        $mergedColValues = array_merge($colValues, $whereColValues);
        return $this->query($sql, $mergedColValues);
    }

}

?>