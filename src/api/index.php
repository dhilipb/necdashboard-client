<?php
header("Content-type: application/json");
session_start();

require_once "./config.php";
require_once "./database.php";

// API Managers
require_once "./api/api-manager.php";
require_once "./api/dashboard-api-manager.php";
require_once "./api/widget-api-manager.php";
require_once "./api/widget-data-api-manager.php";
require_once "./api/widget-settings-api-manager.php";
require_once "./api/user-settings-api-manager.php";
require_once "./api/user-api-manager.php";

// Utils
require_once "./util/api-util.php";
require_once "./util/string-util.php";
require_once "./util/user-util.php";

header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Origin: http://localhost:4200");
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    header("Access-Control-Allow-Headers: Authorization, Content-Type, Accept, Origin");
    exit(0);
}

$apiManager = new ApiManager();

$json = null;
if (isset($_GET["requests"])) {
    $json = json_decode($_GET["requests"]);
} else {
    $json = json_decode(file_get_contents('php://input'));
}

// Default output
$output = array(
    "status" => false,
    "response" => [$json]
);

if ($json != null && isset($json->requests)) {
    $requests = $json->requests;
    if (is_array($requests)) {
        $output = array(
            "status" => true,
            "requests" => $requests,
            "response" => []
        );
        foreach ($requests as $request) {
            try {
                $apiResult = $apiManager->processRequest($request);
                $output['status'] = $output['status'] && $apiResult !== null;
                array_push($output['response'], $apiResult);
            } catch (Exception $e) {
                $output['status'] = false;
                $output['error'] = $e->getMessage();
            }
        }
    }
}

echo json_encode($output);