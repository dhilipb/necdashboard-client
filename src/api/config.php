<?php
// MUTE NOTICES
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_NOTICE);
// error_reporting(E_ALL);

// DATABASE SETTINGS
define("MSSQL_LOCATION", getenv("MSSQL_LOCATION"));
define("MSSQL_HOST", getenv('MSSQL_HOST'));
define("MSSQL_USER", getenv('MSSQL_USER'));
define("MSSQL_PASS", getenv('MSSQL_PASS'));

define("NEOFACE_DB", getenv('NEOFACE_DB') ?: 'NeoFace2');
define("NECDASHBOARD_DB", getenv('NECDASHBOARD_DB') ?: 'NECDashboard');


?>