<?php

class UserApiManager {

    private $database;
    function __construct($database) {
        $this->database = $database;
    }

    function processRequest($request) {
        switch ($request->action) {
            case "login":
                return $this->login($request->args);
            case "logout":
                return $this->logout();
            case "isAuthenticated":
                return $this->isAuthenticated();
        }
        return null;
    }

    function login($args) {
        $username = $args->username;
        $password = sha1($args->username . '_' . $args->password);
        $data = $this->database->fetchFirst("SELECT * FROM db_user WHERE username = ? AND password = ? AND enabled = 1", [$username, $password]);
        if ($data != null) {
            $this->logout();
            session_start();
            $_SESSION["username"] = $username;
            $_SESSION["user"] = $data;

            // Add to login log
            $this->database->insert("db_loginlogs", array(
                "userid" => $data["id"],
                "browser" => $_SERVER['HTTP_USER_AGENT'],
                "ipaddress" => $this->getUserIpAddress(),
                "hostname" => gethostname()
            ));

            return true;
        }

        return false;
    }

    function logout() {
        if (isset($_SESSION["username"])) {
            session_unset();
            session_destroy();
        }
        return true;
    }

    function isAuthenticated() {
        return isset($_SESSION["username"]);
    }

    private function getUserIpAddress() {
        if(!empty($_SERVER['HTTP_CLIENT_IP'])) {
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

}

?>