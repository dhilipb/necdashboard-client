<?php

class DashboardApiManager {

    private $database;
    function __construct($database) {
        $this->database = $database;
    }

    function processRequest($request) {
        $args = $request->args;

        if ($request->action == "all") {
            return $this->all();
        }

        if (ApiRequestUtil::isCreate($request)) {
            return $this->create($request, $args);
        } else if (ApiRequestUtil::isGet($request)) {
            return $this->get($request, $args);
        } else if (ApiRequestUtil::isEdit($request)) {
            return $this->edit($request, $args);
        } else if (ApiRequestUtil::isDelete($request)) {
            return $this->delete($request, $args);
        }
    }

    function get($request, $args) {
        return $this->database->fetch("SELECT * FROM db_dashboard WHERE userid = :userId AND enabled = 1", array("userId" => UserUtil::getUserId()));
    }

    function create($request, $args) {
        ApiUtil::requireArgs($request, ["name"]);

        return $this->database->insert("db_dashboard", array(
            "name" => $args->name,
            "userid" => UserUtil::getUserId(),
            "enabled" => 1,
            "locked" => 0
        ));
    }

    function edit($request, $args) {
        ApiUtil::requireArgs($request, ["name", "enabled", "locked", "dashboardId"]);

        $hasOwnership = $this->database->contains("SELECT * FROM db_dashboard WHERE id = ? AND userid = ?", [$args->dashboardId, UserUtil::getUserId()]);
        if (!$hasOwnership) {
            throw new Exception("Unauthorized. No ownership");
        }

        return $this->database->update("db_dashboard", array(
            "name" => $args->name,
            "enabled" => $args->enabled,
            "locked" => $args->locked,
        ), "id = :id", array(
            "id" => $args->dashboardId
        ));
    }

    function delete($request, $args) {
        ApiUtil::requireArgs($request, ["dashboardId"]);

        $hasOwnership = $this->database->contains("SELECT * FROM db_dashboard WHERE id = ? AND userid = ?", [$args->dashboardId, UserUtil::getUserId()]);
        if (!$hasOwnership) {
            throw new Exception("Unauthorized. No ownership");
        }

        $this->database->query("DELETE db_wsettings WHERE wid IN (SELECT wid FROM db_widget WHERE dashid = ?)", [$args->dashboardId]);
        $this->database->query("DELETE db_widget WHERE dashid = ?", [$args->dashboardId]);
        $this->database->query("DELETE db_dashboard WHERE id = ?", [$args->dashboardId]);
        return true;
    }


    function all() {
        return $this->database->fetch(
            "SELECT d.id as dashboardId, w.id as widgetId, ws.id as widgetSettingsId, *
            FROM db_dashboard d
            INNER JOIN db_widget w
            ON d.id = w.dashid
            LEFT JOIN db_wsettings ws
            ON w.id = ws.wid
            WHERE d.userid = ? AND d.enabled = 1", [UserUtil::getUserId()]
        );
    }

}

?>