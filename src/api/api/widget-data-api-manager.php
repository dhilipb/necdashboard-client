<?php

class WidgetDataApiManager {

    private $database;
    function __construct($database) {
        $this->database = $database;
    }


    function processRequest($request) {
        $args = $request->args;
        
        if (ApiRequestUtil::isCreate($request)) {
            return $this->create($request, $args);
        } else if (ApiRequestUtil::isGet($request)) {
            return $this->get($request, $args);
        } else if (ApiRequestUtil::isEdit($request)) {
            return $this->edit($request, $args);
        } else if (ApiRequestUtil::isDelete($request)) {
            return $this->delete($request, $args);
        } else if (ApiRequestUtil::isUpsert($request)) {
            return $this->upsert($request, $args);
        }
    }
    function get($request, $args) {
        if ($args->widgetid) {
            return $this->database->fetch("SELECT * FROM db_widgetdata WHERE widgetid = ? AND userid = ?", [$args->widgetid, UserUtil::getUserId()]);
        }
        return $this->database->fetch("SELECT * FROM db_widgetdata WHERE userid = ?", [UserUtil::getUserId()]);
    }
    function create($request, $args) {
        ApiUtil::requireArgs($request, ["widgetid", "key", "value"]);

        return $this->database->insert("db_widgetdata", array(
            "userid" => UserUtil::getUserId(),
            "widgetid" => $args->widgetid,
            "key" => $args->key,
            "value" => $args->value
        ));
    }
    function edit($request, $args) {
        ApiUtil::requireArgs($request, ["id", "key", "value"]);

        $hasOwnership = $this->database->contains("SELECT * FROM db_widgetdata WHERE id = ? AND userid = ?", [$args->id, UserUtil::getUserId()]);
        if (!$hasOwnership) {
            throw new Exception("Unauthorized. No ownership");
        }

        return $this->database->update("db_widgetdata", array(
            "key" => $args->key,
            "value" => $args->value
        ), "id = :id", array(
            "id" => $args->id
        ));
    }
    function upsert($request, $args) {
        ApiUtil::requireArgs($request, ["widgetid", "key", "value"]);

        $hasOwnership = $this->database->contains("SELECT * FROM db_widgetdata WHERE `key` = ? AND widgetid = ? AND userid = ?", [$args->key, $args->widgetid, UserUtil::getUserId()]);
        if (!$hasOwnership) {
            throw new Exception("Unauthorized. No ownership");
        }

        $containsKey = $this->database->fetchFirst("SELECT id FROM db_widgetdata WHERE `key` = ? AND userid = ?", [$args->key, UserUtil::getUserId()]);
        if ($containsKey) {
            $args->id = $containsKey["id"];
            $this->edit($request, $args);
        } else {
            $this->create($request, $args);
        }
    }
    function delete($request, $args) {
        ApiUtil::requireArgs($request, ["id"]);

        $hasOwnership = $this->database->contains("SELECT * FROM db_widgetdata WHERE id = ? AND userid = ?", [$args->id, UserUtil::getUserId()]);
        if (!$hasOwnership) {
            throw new Exception("Unauthorized. No ownership");
        }

        $this->database->query("DELETE db_widgetdata WHERE id = ?", [$args->id]);
        return true;
    }

}

?>