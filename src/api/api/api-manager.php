<?php

class ApiManager {

    private $database;
    private $userApiManager;
    private $dashboardApiManager;
    private $widgetApiManager;
    private $widgetSettingsApiManager;
    private $userSettingsApiManager;
    private $widgetDataApiManager;

    function __construct() {
        $this->database = new DatabaseClient(NEOFACE_DB);

        $necDashboardDatabase = new DatabaseClient(NECDASHBOARD_DB);
        $this->userApiManager = new UserApiManager($necDashboardDatabase);
        $this->dashboardApiManager = new DashboardApiManager($necDashboardDatabase);
        $this->widgetApiManager = new WidgetApiManager($necDashboardDatabase);
        $this->widgetSettingsApiManager = new WidgetSettingsApiManager($necDashboardDatabase);
        $this->userSettingsApiManager = new UserSettingsApiManager($necDashboardDatabase);
        $this->widgetDataApiManager = new WidgetDataApiManager($necDashboardDatabase);
    }

    function processRequest($request) {
        switch ($request->entity) {
            default:
                return null;

            case "user":
                return $this->userApiManager->processRequest($request);

            case "dashboard":
                UserUtil::checkIsAuthenticated();
                return $this->dashboardApiManager->processRequest($request);

            case "widget":
                UserUtil::checkIsAuthenticated();
                return $this->widgetApiManager->processRequest($request);

            case "widgetSettings":
                UserUtil::checkIsAuthenticated();
                return $this->widgetSettingsApiManager->processRequest($request);
                        
            case "widgetData":
                UserUtil::checkIsAuthenticated();
                return $this->widgetDataApiManager->processRequest($request);
                        
            case "userSettings":
                UserUtil::checkIsAuthenticated();
                return $this->userSettingsApiManager->processRequest($request);
                            
            case "db-views":
                return $this->getViews();

            case "cameras":
                return $this->getCameras();

            case "view":
                return $this->getData($request->args);

            case "count":
                return $this->getCount($request->args);

            case "chart-data":
                return $this->getChartData($request->args);

            case "process-stats":
                return $this->getProcessStats();

            case "watchlist":
                return $this->getWatchlists();
        }
    }

    function getViews() {
        UserUtil::checkIsAuthenticated();

        return $this->database->fetch("SELECT name FROM sys.views ORDER BY name");
    }

    function getData($args) {
        UserUtil::checkIsAuthenticated();
        $viewName = $args->viewName;
        if ($this->isView($viewName)) {
            switch ($viewName) {
                case "LastMatchWidgetViewIS":
                    return $this->database->fetch("SELECT TOP 10 * FROM dbo.$viewName ORDER BY CaptureTime DESC");
                default:
                    return $this->database->fetch("SELECT * FROM dbo." . $viewName);
            }
        }
        return null;
    }

    function getCount($args) {
        UserUtil::checkIsAuthenticated();
        $viewName = $args->viewName;
        switch ($viewName) {
            case "ISGenderCountMale": 
                return $this->database->fetch("SELECT * from dbo.ISGenderCount WHERE Gender = 'Male'");
            case "ISGenderCountFemale": 
                return $this->database->fetch("SELECT * from dbo.ISGenderCount WHERE Gender = 'Female'");
            default:
                if ($this->isView($viewName)) {
                    return $this->database->fetch("SELECT * from dbo." . $viewName);
                }
                return null;
        }
    }

    function getChartData($args) {
        UserUtil::checkIsAuthenticated();
        $chartType = $args->chartType;
        switch ($chartType) {
            case "ISScoreCount":
                return $this->database->fetch("SELECT * from dbo.$chartType ORDER BY Range ASC");
            default:
                if (strpos($chartType, 'IS') === 0) {
                    return $this->database->fetch("SELECT * from dbo." . $chartType);
                }
                return null;
        }
    }

    function getProcessStats() {
        UserUtil::checkIsAuthenticated();

        return $this->database->fetch("SELECT * from dbo.ISProcessStats");
    } 

    function getCameras() {
        UserUtil::checkIsAuthenticated();

        return $this->database->fetch("SELECT * from dbo.Cameras WHERE AutoCapture = 1");
    }

    function getWatchlists() {
        UserUtil::checkIsAuthenticated();

        return $this->database->fetch("SELECT * from dbo.Watchlists WHERE HideFromGUI = 0");
    }

    function isView($view) {
        $view = addslashes($view);
        return StringUtil::startsWith($view, 'IS') || StringUtil::endsWith($view, 'IS');
    }

}
?>