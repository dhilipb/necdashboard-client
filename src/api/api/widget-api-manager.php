<?php

class WidgetApiManager {

    private $database;
    function __construct($database) {
        $this->database = $database;
    }


    function processRequest($request) {
        $args = $request->args;
        
        if (ApiRequestUtil::isCreate($request)) {
            return $this->create($request, $args);
        } else if (ApiRequestUtil::isGet($request)) {
            return $this->get($request, $args);
        } else if (ApiRequestUtil::isEdit($request)) {
            return $this->edit($request, $args);
        } else if (ApiRequestUtil::isDelete($request)) {
            return $this->delete($request, $args);
        }
    }
    function get($request, $args) {
        ApiUtil::requireArgs($request, ["dashboardId"]);
        return $this->database->fetch("SELECT * FROM db_widget WHERE dashid = ?", [$args->dashboardId]);
    }
    function create($request, $args) {
        ApiUtil::requireArgs($request, ["type", "dashboardId", "x", "y", "cols", "rows"]);

        return $this->database->insert("db_widget", array(
            "type" => $args->type,
            "dashid" => $args->dashboardId,
            "x" => $args->x,
            "y" => $args->y,
            "cols" => $args->cols,
            "rows" => $args->rows,
            "userid" => UserUtil::getUserId()
        ));
    }
    function edit($request, $args) {
        ApiUtil::requireArgs($request, ["x", "y", "cols", "rows", "widgetId"]);

        $hasOwnership = $this->database->contains("SELECT * FROM db_widget WHERE id = ? AND userid = ?", [$args->widgetId, UserUtil::getUserId()]);
        if (!$hasOwnership) {
            throw new Exception("Unauthorized. No ownership");
        }

        return $this->database->update("db_widget", array(
            "x" => $args->x,
            "y" => $args->y,
            "cols" => $args->cols,
            "rows" => $args->rows
        ), "id = :widgetId", array(
            "widgetId" => $args->widgetId
        ));
    }
    function delete($request, $args) {
        ApiUtil::requireArgs($request, ["widgetId"]);

        $hasOwnership = $this->database->contains("SELECT * FROM db_widget WHERE id = ? AND userid = ?", [$args->widgetId, UserUtil::getUserId()]);
        if (!$hasOwnership) {
            throw new Exception("Unauthorized. No ownership");
        }

        $this->database->query("DELETE db_wsettings WHERE wid = ?", [$args->widgetId]);
        $this->database->query("DELETE db_widget WHERE id = ?", [$args->widgetId]);
        return true;
    }

}

?>