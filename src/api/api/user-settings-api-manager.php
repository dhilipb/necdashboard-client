<?php

class UserSettingsApiManager {

    private $database;
    function __construct($database) {
        $this->database = $database;
    }


    function processRequest($request) {
        $args = $request->args;
        
        if (ApiRequestUtil::isCreate($request)) {
            return $this->create($request, $args);
        } else if (ApiRequestUtil::isGet($request)) {
            return $this->get($request, $args);
        } else if (ApiRequestUtil::isEdit($request)) {
            return $this->edit($request, $args);
        } else if (ApiRequestUtil::isDelete($request)) {
            return $this->delete($request, $args);
        } else if (ApiRequestUtil::isUpsert($request)) {
            return $this->upsert($request, $args);
        }
    }
    function get($request, $args) {
        return $this->database->fetch("SELECT * FROM db_usersettings WHERE userid = ?", [UserUtil::getUserId()]);
    }
    function create($request, $args) {
        ApiUtil::requireArgs($request, ["key", "value"]);

        return $this->database->insert("db_usersettings", array(
            "userid" => UserUtil::getUserId(),
            "key" => $args->key,
            "value" => $args->value
        ));
    }
    function edit($request, $args) {
        ApiUtil::requireArgs($request, ["id", "key", "value"]);

        $hasOwnership = $this->database->contains("SELECT * FROM db_usersettings WHERE id = ? AND userid = ?", [$args->id, UserUtil::getUserId()]);
        if (!$hasOwnership) {
            throw new Exception("Unauthorized. No ownership");
        }

        return $this->database->update("db_usersettings", array(
            "key" => $args->key,
            "value" => $args->value
        ), "id = :id", array(
            "id" => $args->id
        ));
    }
    function upsert($request, $args) {
        ApiUtil::requireArgs($request, ["key", "value"]);

        $hasOwnership = $this->database->contains("SELECT * FROM db_usersettings WHERE `key` AND userid = ?", [$args->key, UserUtil::getUserId()]);
        if (!$hasOwnership) {
            throw new Exception("Unauthorized. No ownership");
        }

        $containsKey = $this->database->fetchFirst("SELECT id FROM db_usersettings WHERE `key` = ? AND userid = ?", [$args->key, UserUtil::getUserId()]);
        if ($containsKey) {
            $args->id = $containsKey["id"];
            $this->edit($request, $args);
        } else {
            $this->create($request, $args);
        }
    }
    function delete($request, $args) {
        ApiUtil::requireArgs($request, ["id"]);

        $hasOwnership = $this->database->contains("SELECT * FROM db_usersettings WHERE id = ? AND userid = ?", [$args->id, UserUtil::getUserId()]);
        if (!$hasOwnership) {
            throw new Exception("Unauthorized. No ownership");
        }

        $this->database->query("DELETE db_usersettings WHERE id = ?", [$args->id]);
        return true;
    }

}

?>