<?php

class WidgetSettingsApiManager {

    private $database;
    function __construct($database) {
        $this->database = $database;
    }

    function processRequest($request) {
        $args = $request->args;

        if (ApiRequestUtil::isCreate($request)) {
            return $this->create($request, $args);
        } else if (ApiRequestUtil::isGet($request)) {
            return $this->get($request, $args);
        } else if (ApiRequestUtil::isEdit($request)) {
            return $this->edit($request, $args);
        } else if (ApiRequestUtil::isDelete($request)) {
            return $this->delete($request, $args);
        }
    }
    function get($request, $args) {
        ApiUtil::requireArgs($request, ["widgetSettingsId"]);
        return $this->database->fetch("SELECT * FROM db_wsettings WHERE wid = ?", [$args->widgetSettingsId]);
    }
    function create($request, $args) {
        ApiUtil::requireArgs($request, ["key", "value", "widgetId"]);

        $this->database->query("DELETE db_wsettings WHERE [key] = ? and [wid] = ?", [$args->key, $args->widgetId]);
        return $this->database->insert("db_wsettings", array(
            "key" => $args->key,
            "value" => $args->value,
            "wid" => $args->widgetId,
            "userid" => UserUtil::getUserId()
        ));
    }
    function edit($request, $args) {
        ApiUtil::requireArgs($request, ["widgetSettingsId"]);

        $hasOwnership = $this->database->contains("SELECT * FROM db_wsettings WHERE id = ? AND userid = ?", [$args->widgetSettingsId, UserUtil::getUserId()]);
        if (!$hasOwnership) {
            throw new Exception("Unauthorized. No ownership");
        }

        ApiUtil::requireArgs($request, ["widgetSettingsId", "key", "value"]);

        return $this->database->update("db_wsettings", array(
            "key" => $args->key,
            "value" => $args->value,
        ), "id = :widgetSettingsId", array(
            "widgetSettingsId" => $args->widgetSettingsId
        ));
    }
    function delete($request, $args) {
        ApiUtil::requireArgs($request, ["widgetSettingsId"]);

        $hasOwnership = $this->database->contains("SELECT * FROM db_wsettings WHERE id = ? AND userid = ?", [$args->widgetSettingsId, UserUtil::getUserId()]);
        if (!$hasOwnership) {
            throw new Exception("Unauthorized. No ownership");
        }

        return $this->database->fetch("DELETE db_wsettings WHERE [id] = ?", [$args->widgetSettingsId]);
    }
}

?>