# PHP API

## Requests

```json
{
  "request": [
    {
      "entity": "view",
      "method": "get",
      "args": {
        "viewName": "ISMatchCount"
      }
    },
    {
      "entity": "count",
      "method": "get",
      "args": {
        "countType": "ISMatchCount"
      }
    },
    {
      "entity": "watchlist",
      "method": "get",
      "args": {}
    },
    {
      "entity": "user",
      "method": "get",
      "action": "login",
      "args": {
        "username": "everything",
        "password": "nothing"
      }
    }
  ]
}
```

## Response

```json
{
  "success": true,
  "response": [
    {
      "entity": "view",
      "method": "get",
      "args": {
      "viewName": "ISMatchCount",
      "success": true,
      "data": []
    }
  ]
}
```
