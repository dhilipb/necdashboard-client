<?php

class ApiRequestUtil {
    static function isGet($request) {
        return $request->method === 'get';
    }
    static function isEdit($request) {
        return $request->method === 'edit';
    }
    static function isDelete($request) {
        return $request->method === 'delete';
    }
    static function isCreate($request) {
        return $request->method === 'create';
    }
    static function isUpsert($request) {
        return $request->method === 'upsert';
    }
}


class ApiUtil {
    static function requireArgs($request, $args) {
        $requiredArgs = [];
        foreach ($args as $arg) {
            if (!property_exists($request->args, $arg)) {
                array_push($requiredArgs, $arg);
            }
        }
        if (!empty($requiredArgs)) {
            throw new Exception("Args " . implode(",", $requiredArgs) + " required");
        }
    }

}


?>