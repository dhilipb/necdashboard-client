<?php

class UserUtil {
    public static function checkIsAuthenticated() {
        if (!isset($_SESSION["username"])) {
            throw new Exception("Unauthorized");
        }
    }

    public static function getUser() {
        return $_SESSION["user"];
    }

    public static function getUserId() {
        return $_SESSION["user"]["id"];
    }
}
?>