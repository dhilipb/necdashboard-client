import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ViewCameraComponent } from './view-camera.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{path: '', component: ViewCameraComponent}])
  ],
  declarations: [ViewCameraComponent]
})
export class ViewCameraModule { }
