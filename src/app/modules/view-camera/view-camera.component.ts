import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Camera } from '@core/model';
import { CameraService } from '@core/services';

@Component({
  selector: 'dash-view-camera',
  templateUrl: './view-camera.component.html',
  styleUrls: ['./view-camera.component.scss']
})
export class ViewCameraComponent implements OnInit {

  public camera: Camera;

  constructor(
    private route: ActivatedRoute,
    private cameraService: CameraService
  ) { }

  ngOnInit() {

  }

}
