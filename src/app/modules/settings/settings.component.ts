import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AppConstants, UserConfigService } from '@core/config';
import { CameraService, WatchlistService } from '@core/services';
import { untilDestroyed } from '@core/until-destroy';
import { get } from 'lodash';

@Component({
  selector: 'dash-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {

  public saveStatus: string = 'Save';
  public cameras: string[] = [];
  public watchlists: string[] = ['VIP'];

  public settingsForm: FormGroup = new FormGroup({});

  constructor(
    private userConfigService: UserConfigService,
    private cameraService: CameraService,
    private watchlistService: WatchlistService
  ) { }

  ngOnInit() {
    this.userConfigService.loadConfig();

    Object.entries(AppConstants.UserSettings).forEach(([key, value]) => {
      if (typeof value === 'boolean') {
        value = value ? 'true' : 'false';
      }
      this.settingsForm.registerControl(key, new FormControl(value));
    });

    this.cameraService.getCameras()
      .pipe(untilDestroyed(this))
      .subscribe(output => {
        this.cameras = get(output, 'response');
      });

    this.watchlistService.getWatchlists().pipe(untilDestroyed(this)).subscribe(output => {
      this.watchlists = get(output, 'response');
    });
  }

  ngOnDestroy() { }

  onSubmit() {
    this.saveStatus = 'Saving';
    this.userConfigService.persistConfig(this.settingsForm);
    this.saveStatus = 'Saved';
    window.location.reload();
  }

  reset() {
    if (confirm('Are you sure you would like to reset all values?')) {
      localStorage.clear();
      window.location.reload();
    }
  }

}
