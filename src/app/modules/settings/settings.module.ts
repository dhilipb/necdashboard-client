import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { GridsterModule } from 'angular-gridster2';

import { SharedModule } from '../../shared/shared.module';
import { DashboardSettingsComponent } from './dashboard-settings/dashboard-settings.component';
import { SettingsComponent } from './settings.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    GridsterModule,
    SharedModule,
    RouterModule.forChild([{ path: '', component: SettingsComponent }]),
    FontAwesomeModule
  ],
  declarations: [SettingsComponent, DashboardSettingsComponent]
})
export class SettingsModule { }
