import { Component, OnInit } from '@angular/core';
import { ApiEntity, DashboardDto } from '@core/model';
import { ApiService } from '@core/services';
import { faEdit, faTrash, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { get } from 'lodash';

@Component({
  selector: 'setting-dashboard',
  templateUrl: './dashboard-settings.component.html',
  styleUrls: ['./dashboard-settings.component.scss']
})
export class DashboardSettingsComponent implements OnInit {
  public dashboards: DashboardDto[];

  public showLoadingAdd: boolean = false;
  public showLoadingDashboards: boolean = true;

  public faEdit: IconDefinition = faEdit;
  public faTrash: IconDefinition = faTrash;
  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {

    this.apiService.get(ApiEntity.DASHBOARD).subscribe(result => {
      const dashboardsResponse: DashboardDto[] = get(result, 'response', []);
      this.dashboards = dashboardsResponse;
      this.showLoadingDashboards = false;
    });
  }

  public renameDashboard(dashboard: DashboardDto): void {
    const newDashboardName = prompt('What would you like to rename this dashboard to?');
    if (newDashboardName) {
      this.showLoadingDashboards = true;
      this.apiService.edit(ApiEntity.DASHBOARD, {
        dashboardId: dashboard.id,
        enabled: dashboard.enabled,
        locked: dashboard.locked,
        name: newDashboardName
      }).subscribe(response => {
        this.showLoadingDashboards = false;
        window.location.reload();
      });
    }
  }
  public deleteDashboard(dashboard: DashboardDto): void {
    if (confirm('Are you sure you wish to delete this dashboard? This will delete all the widgets in the dashboard.')) {
      this.showLoadingDashboards = false;
      // THIS IS DANGEROUS!
      this.apiService.delete(ApiEntity.DASHBOARD, {
        dashboardId: dashboard.id
      }).subscribe(response => {
        window.location.reload();
      });
      console.log('Delete initiated');
    }
  }
  public addDashboard(addInputElem: any): void {
    if (addInputElem && addInputElem.value) {
      const dashboardName = addInputElem.value;
      if (confirm(`Are you sure you wish to add a new dashboard called '${dashboardName}'`)) {
        this.showLoadingAdd = true;
        this.apiService.create(ApiEntity.DASHBOARD, {
          name: dashboardName
        }).subscribe(result => {
          this.showLoadingAdd = false;
          window.location.reload();
        });
      }
    } else {
      alert('Please enter a name to create');
    }
  }

}
