import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiEntity } from '@core/model';
import { ApiService, LocalCacheService, StoreService } from '@core/services';
import { DialogConfig, DialogRef } from '@shared/dialog';
import { GridsterItem } from 'angular-gridster2';
import { get } from 'lodash';

import { WidgetComponentsNames } from '../../widget-components';

@Component({
  selector: 'app-add-widget-dialog',
  templateUrl: './add-widget-dialog.component.html',
  styleUrls: ['./add-widget-dialog.component.scss']
})
export class AddWidgetDialogComponent implements OnInit {

  public addWidgetForm: FormGroup = new FormGroup({
    dashboard: new FormControl('', Validators.required),
    widgetType: new FormControl('', Validators.required),
    widgetTitle: new FormControl('', Validators.required)
  });
  public submitted: boolean = false;
  public submitting: boolean = false;

  public dashboards: Array<string> = [];
  public currentDashboard: string;

  public widgetTypes: any = WidgetComponentsNames;

  constructor(
    private dialogConfig: DialogConfig,
    private dialogRef: DialogRef,
    private localCacheService: LocalCacheService,
    private apiService: ApiService,
    private router: Router,
    private storeService: StoreService
  ) {

  }

  ngOnInit() {
    this.initDashboardList();
  }

  private initDashboardList() {
    this.dashboards = this.localCacheService.get('dashboard');
    this.currentDashboard = get(this.dialogConfig, 'data.dashboardName');
  }

  onSubmit() {
    this.submitted = true;

    // Create widget
    if (this.addWidgetForm.valid) {
      this.submitting = true;

      const type = this.addWidgetForm.controls.widgetType.value;
      const widgetTitle = this.addWidgetForm.controls.widgetTitle.value;
      const widgetItem = {
        dashboardId: this.addWidgetForm.controls.dashboard.value,
        type,
        x: 0,
        y: 100,
        cols: 5,
        rows: 5,
      } as GridsterItem;

      this.apiService.create(ApiEntity.WIDGET, widgetItem).subscribe(result => {
        const widgetId = get(result, 'response');
        this.apiService.create(ApiEntity.WIDGET_SETTINGS, {
          key: 'widgetTitle',
          widgetId,
          value: widgetTitle
        }).subscribe(data => {
          this.router.navigate(['.']);
          setTimeout(() => {
            window.location.reload();
          }, 500);
        });
      });
    }
  }

}
