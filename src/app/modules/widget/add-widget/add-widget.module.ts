import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';

import { AddWidgetDialogComponent } from './add-widget-dialog/add-widget-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [AddWidgetDialogComponent],
  entryComponents: [AddWidgetDialogComponent]
})
export class AddWidgetModule { }
