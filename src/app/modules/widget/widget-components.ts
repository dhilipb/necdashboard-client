import { AwayAlertWidgetComponent, CameraMapComponent, ChartWidgetComponent, CountWidgetComponent, LastHitWidgetComponent, LiveCamerasWidgetComponent, LogosWidgetComponent, RecentVisitorsWidgetComponent, TableDataWidgetComponent } from './widgets';

export const WidgetComponents = {
    LiveCamerasWidgetComponent,
    CameraMapComponent,
    LastHitWidgetComponent,
    RecentVisitorsWidgetComponent,
    CountWidgetComponent,
    LogosWidgetComponent,
    ChartWidgetComponent,
    TableDataWidgetComponent,
    AwayAlertWidgetComponent
};

export const WidgetComponentsList = Object.values(WidgetComponents);

export const WidgetComponentsNames = {
    LiveCamerasWidgetComponent: 'Live Cameras',
    CameraMapComponent: 'Camera Map',
    LastHitWidgetComponent: 'Last Hit',
    RecentVisitorsWidgetComponent: 'Recent Visitors',
    CountWidgetComponent: 'Custom Count',
    ChartWidgetComponent: 'Custom Chart',
    LogosWidgetComponent: 'Powered By',
    TableDataWidgetComponent: 'Table Data',
    AwayAlertWidgetComponent: 'Away Alert'
};
