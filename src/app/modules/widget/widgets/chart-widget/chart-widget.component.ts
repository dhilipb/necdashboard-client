import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { WidgetComponent, WidgetComponentOptions } from '@core/model';
import { untilDestroyed } from '@core/until-destroy';
import { GridsterItemComponentInterface } from 'angular-gridster2';
import * as Highcharts from 'highcharts';
import { get } from 'lodash';

import { WidgetSettingsService } from '../../options/services/widget-settings.service';
import { WidgetDataService } from '../services';
import { WidgetService } from '../services/widget.service';

@Component({
  selector: 'widget-chart',
  templateUrl: './chart-widget.component.html',
  styleUrls: ['./chart-widget.component.scss']
})
export class ChartWidgetComponent implements OnInit, OnDestroy, WidgetComponent {

  Highcharts: typeof Highcharts = Highcharts;

  chartOptions: Highcharts.Options = {
    series: [{
      name: 'Count',
      type: 'column',
      data: [21, 88, 43, 14, 35],
    }],
    title: {
      text: ''
    },
    plotOptions: {
      pie: {
        dataLabels: {
          enabled: false
        },
        showInLegend: true
      },
      column: {
        borderWidth: 0,
        showInLegend: false
      },
      bar: {
        borderWidth: 0,
        showInLegend: false
      }
    },
    chart: {
      backgroundColor: 'transparent',
      reflow: true,
      style: {
        fontFamily: 'Arial, Helvetica, sans-serif'
      }
    },
    legend: {
      // enabled: false,
      layout: 'vertical',
      align: 'right',
      itemStyle: {
        color: 'white'
      }
    },
    xAxis: {
      categories: [],
      gridLineWidth: 0,
      title: {
        text: ''
      },
      labels: {
        style: {
          color: 'white'
        }
      }
    },
    yAxis: {
      gridLineWidth: 0,
      title: {
        text: ''
      },
      min: 0,
      labels: {
        style: {
          color: 'white'
        }
      }
    },
    credits: {
      enabled: false
    },
    colors: ['#72AC4C', '#E57431', '#FCBE2C', '#4675C2', '#4675C2', '#A5A5A5']
  };
  shouldUpdate: boolean = false;
  public loading: boolean = true;

  constructor(
    @Inject('widgetId') private widgetId: string,
    private widgetDataService: WidgetDataService,
    private widgetService: WidgetService,
    private widgetSettingsService: WidgetSettingsService
  ) { }

  ngOnInit() {
    const dataColumn = this.widgetSettingsService.retrieveKey(this.widgetId, 'dataColumn');
    const dataView = this.widgetSettingsService.retrieveKey(this.widgetId, 'dataView');
    this.widgetDataService.getChartData(dataView, this).subscribe(result => {
      const data: Array<any> = get(result, 'response');
      if (data && data.length > 0) {
        const labelKey = Object.keys(data[0]).filter(x => x !== dataColumn).join();

        // Update data
        this.chartOptions.series[0]['data'] = data.map(entry => ({
          name: entry[labelKey],
          y: parseInt(get(entry, dataColumn, '0'), 10)
        }));

        // Get xAxis labels
        this.chartOptions.xAxis['categories'] = data.map(entry => entry[labelKey]);

        this.shouldUpdate = true;
        this.loading = false;
      }
    });

    this.widgetService.onResizeWidget.pipe(untilDestroyed(this)).subscribe(itemComponent => {
      if (itemComponent && itemComponent.item.widgetId === this.widgetId) {
        this.onResizeWidget(itemComponent);
      }
    });

    this.updateChartType();
    this.updateColours();
  }

  private updateChartType() {
    const chartType = this.widgetSettingsService.retrieveKey(this.widgetId, 'chartType');
    if (chartType) {
      this.chartOptions.series.forEach(s => {
        s.type = chartType;
        if (s.type === 'pie') {
          s.borderWidth = 1;
        }
      });
    }
  }

  private updateColours() {
    const chartColours = this.widgetSettingsService.retrieveKey(this.widgetId, 'chartColour');
    let hexColours;
    switch (chartColours) {
      case 'green':
        hexColours = '#72AC4C';
        break;
      case 'orange':
        hexColours = '#E57431';
        break;
      case 'blue':
        hexColours = '#4675C2';
        break;
      default:
      case 'multi-colour':
        hexColours = '#72AC4C,#E57431,#FCBE2C,#4675C2,#4675C2,#A5A5A5';
        break;
    }
    this.chartOptions.colors = hexColours.split(',');
  }

  ngOnDestroy() { }

  chartCallback(chart: any) {
    this.shouldUpdate = true;
  }

  private onResizeWidget(itemComponent: GridsterItemComponentInterface) {
    if (itemComponent.width && itemComponent.height) {
      this.chartOptions.chart.width = Math.floor(itemComponent.width - 10);
      this.chartOptions.chart.height = Math.floor(itemComponent.height - 45) + 'px';
      this.shouldUpdate = true;
    }
  }

  public getOptions(): WidgetComponentOptions {
    return {
      widgetTitle: { value: 'Hourly Visitors' },
      dataView: {
        title: 'Database View',
        type: 'db-views',
        value: 'ISHourCount'
      },
      dataColumn: {
        title: 'Data column',
        value: 'Count'
      },
      chartType: {
        title: 'Chart Type',
        type: 'select',
        value: 'column',
        options: [
          { title: 'Column', value: 'column' },
          { title: 'Bar', value: 'bar' },
          { title: 'Pie', value: 'pie' },
        ]
      },
      chartColour: {
        title: 'Chart colour',
        type: 'select',
        value: 'multi-colour',
        options: [
          { title: 'Green', value: 'green' },
          { title: 'Orange', value: 'orange' },
          { title: 'Blue', value: 'blue' },
          { title: 'Multi-colour', value: 'multi-colour' }
        ]
      }
    };
  }

}
