import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AppConstants } from '@core/config';

@Component({
  selector: 'dash-widget-container',
  templateUrl: './widget-container.component.html',
  styleUrls: ['./widget-container.component.scss']
})
export class WidgetContainerComponent implements OnInit {

  @Output() public onWidgetActionClick: EventEmitter<string> = new EventEmitter<string>();
  public isActionsEnabled: boolean = !AppConstants.UserSettings.Locked;

  public deleting: boolean = false;

  constructor() { }

  ngOnInit() {
  }
  editWidget() {
    this.onWidgetActionClick.emit('edit');
  }
  deleteWidget() {
    if (confirm('Are you sure you wish to delete this widget?')) {
      this.deleting = true;
      this.onWidgetActionClick.emit('delete');
    }
  }

}
