import { Component, Inject, OnInit } from '@angular/core';
import { WidgetComponent, WidgetComponentOptions } from '@core/model';

import { WidgetSettingsService } from '../../options/services/widget-settings.service';

@Component({
  selector: 'widget-logos',
  templateUrl: './logos-widget.component.html',
  styleUrls: ['./logos-widget.component.scss']
})
export class LogosWidgetComponent implements OnInit, WidgetComponent {

  public logos: { path: string; height: string }[] = [];

  constructor(
    @Inject('widgetId') private widgetId: string,
    private widgetSettingsService: WidgetSettingsService
  ) { }

  ngOnInit() {
    const logos = JSON.parse(this.widgetSettingsService.retrieveKey(this.widgetId, 'logoUrls', '[]'));
    this.logos = logos.map(logo => {
      let path = logo;
      let height = 'auto';

      if (logo.includes('|')) {
        const split = logo.split('|');
        path = split[0];
        height = split[1] + 'px';
      }

      return { path, height };
    });
  }

  public getOptions(): WidgetComponentOptions {
    return {
      widgetTitle: { value: 'Powered By' },
      logoUrls: {
        title: 'Logo URLs',
        type: 'list-input',
        subtitle: 'use | to add height',
        value: JSON.stringify(['assets/nec_logo.jpg|100', 'assets/aasa_logo.png|100'])
      }
    };
  }

}

