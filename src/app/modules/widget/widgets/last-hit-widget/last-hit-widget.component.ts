import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { AppConstants } from '@core/config';
import { NecVisitor, WidgetComponent, WidgetComponentOptions } from '@core/model';
import { get } from 'lodash';

import { WidgetSettingsService } from '../../options/services/widget-settings.service';
import { WidgetDataService } from '../services';

@Component({
  selector: 'widget-hit',
  templateUrl: './last-hit-widget.component.html',
  styleUrls: ['./last-hit-widget.component.scss']
})
export class LastHitWidgetComponent implements OnInit, OnDestroy, WidgetComponent {

  public user: NecVisitor;
  public loading: boolean = true;

  constructor(
    @Inject('widgetId') private widgetId: string,
    private widgetSettingsService: WidgetSettingsService,
    private widgetDataService: WidgetDataService
  ) { }

  ngOnInit() {
    const dataView = this.widgetSettingsService.retrieveKey(this.widgetId, 'dataView');
    this.widgetDataService.getData(dataView, this).subscribe(data => {
      this.user = get(data, 'response[0]');
      this.loading = false;
    });
  }

  ngOnDestroy() { }

  getImage(user: any) {
    return AppConstants.UserSettings.NecUrl + '/Image/GetSubjectFaceThumbnail?id=' + user.SubjectPhotoId;
  }

  getCapturedImage(user: any) {
    return AppConstants.UserSettings.NecUrl + '/Image/GetFeaturePhoto?id=' + user.FeatureID;
  }

  public getOptions(): WidgetComponentOptions {
    return {
      widgetTitle: { value: 'Last Hit' },
      dataView: {
        title: 'Database View',
        type: 'db-views',
        value: 'LastMatchWidgetViewIS'
      }
    };
  }
}

