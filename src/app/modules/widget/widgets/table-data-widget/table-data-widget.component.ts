import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { WidgetComponent, WidgetComponentOptions } from '@core/model';
import { untilDestroyed } from '@core/until-destroy';
import { get } from 'lodash';

import { WidgetSettingsService } from '../../options/services/widget-settings.service';
import { WidgetDataService } from '../services';

@Component({
  selector: 'widget-data',
  templateUrl: './table-data-widget.component.html',
  styleUrls: ['./table-data-widget.component.scss']
})
export class TableDataWidgetComponent implements OnInit, OnDestroy, WidgetComponent {

  public columns: Array<string>;
  public data: Array<any> = [];
  public loading: boolean = true;

  constructor(
    @Inject('widgetId') private widgetId: string,
    private widgetDataService: WidgetDataService,
    private widgetSettingsService: WidgetSettingsService
  ) { }

  ngOnInit() {
    this.columns = this.widgetSettingsService.retrieveKey(this.widgetId, 'columns').split(',').map(col => col.trim());
    this.widgetDataService.getProcessStats(this).pipe(untilDestroyed(this)).subscribe(result => {
      this.data = get(result, 'response');
      this.loading = false;
    });
  }

  ngOnDestroy() { }

  getOptions(): WidgetComponentOptions {
    return {
      widgetTitle: {
        value: ''
      },
      columns: {
        title: 'Columns',
        value: 'Service,Camera,TS,CameraFPS,Queue,CPU,Memory,FPGA Load,FPGA Temp'
      }
    };
  }

  isNumber(num: any): boolean {
    return !isNaN(num);
  }

}
