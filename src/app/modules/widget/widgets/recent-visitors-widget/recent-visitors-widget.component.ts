import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { AppConstants } from '@core/config';
import { WidgetComponent, WidgetComponentOptions } from '@core/model';
import { untilDestroyed } from '@core/until-destroy';
import { get } from 'lodash';

import { WidgetSettingsService } from '../../options/services/widget-settings.service';
import { WidgetDataService } from '../services';
import { WidgetService } from '../services/widget.service';

@Component({
  selector: 'widget-visitors',
  templateUrl: './recent-visitors-widget.component.html',
  styleUrls: ['./recent-visitors-widget.component.scss']
})
export class RecentVisitorsWidgetComponent implements OnInit, OnDestroy, WidgetComponent {

  public users: any;
  public shouldShowPicture: boolean = true;
  public loading: boolean = true;

  public containerHeight: number = 100;

  constructor(
    @Inject('widgetId') private widgetId: string,
    private widgetDataService: WidgetDataService,
    private widgetService: WidgetService,
    private widgetSettingsService: WidgetSettingsService
  ) { }

  ngOnInit() {
    const dataView = this.widgetSettingsService.retrieveKey(this.widgetId, 'dataView');
    this.widgetDataService.getData(dataView, this).subscribe(data => {
      this.loading = false;
      const visitors: Array<any> = get(data, 'response');
      const noOfVisitors = this.widgetSettingsService.retrieveKey(this.widgetId, 'noOfVisitors');
      this.users = visitors.slice(0, noOfVisitors);
      this.users.forEach(user => {
        if (user && user.Colour) {
          user.HexColour = '#' + parseInt(user.Colour, 10).toString(16);
        }
      });
    });
    this.shouldShowPicture = this.widgetSettingsService.retrieveKey(this.widgetId, 'showPicture');


    // Set container height on resize
    this.widgetService.onResizeWidget.pipe(untilDestroyed(this)).subscribe(itemComponent => {
      if (itemComponent && itemComponent.item.widgetId === this.widgetId) {
        this.containerHeight = Math.floor(itemComponent.height) - 50;
      }
    });
  }

  ngOnDestroy() { }

  public getOptions(): WidgetComponentOptions {
    return {
      widgetTitle: { value: 'Recent Visitors' },
      showPicture: {
        title: 'Show Picture?',
        value: true
      },
      pictureUrl: {
        title: 'Picture URL',
        value: AppConstants.UserSettings.NecUrl + '/Image/GetSubjectFaceThumbnail?id='
      },
      noOfVisitors: {
        title: 'No. of visitors',
        value: 5
      },
      dataView: {
        title: 'Database View',
        type: 'db-views',
        value: 'LastMatchWidgetViewIS'
      }
    };
  }

  getImage(user: any) {
    return this.widgetSettingsService.retrieveKey(this.widgetId, 'pictureUrl', '') + user.SubjectPhotoId;
  }

}
