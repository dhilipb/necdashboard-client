import { inject, TestBed } from '@angular/core/testing';

import { WidgetService } from './widget.service';

/* tslint:disable:no-unused-variable */

describe('Service: Widget', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WidgetService]
    });
  });

  it('should ...', inject([WidgetService], (service: WidgetService) => {
    expect(service).toBeTruthy();
  }));
});
