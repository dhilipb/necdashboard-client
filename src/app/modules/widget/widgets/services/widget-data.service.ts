import { Injectable, OnDestroy } from '@angular/core';
import { AppConstants } from '@core/config';
import { ApiEntity, ApiRequest } from '@core/model';
import { ApiService } from '@core/services';
import { untilDestroyed } from '@core/until-destroy';
import { get } from 'lodash';
import { Subject, timer } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WidgetDataService {

  public databaseViews: Array<any>;

  constructor(
    private apiService: ApiService
  ) {

  }

  init() {
    this.apiService.request({
      entity: ApiEntity.DB_VIEWS,
    } as ApiRequest)
      .subscribe(result => {
        this.databaseViews = get(result, 'response', []).filter(view => !view['name'].startsWith('vw_')).map(view => view['name']);
        const customViews = ['ISGenderCountMale', 'ISGenderCountFemale'];
        this.databaseViews.push(...customViews);
      });

  }

  getData(viewName: string, component: OnDestroy) {
    return this.pollWidgetData(ApiEntity.VIEW, { viewName }, component);
  }

  getCountData(viewName: string, component: OnDestroy) {
    return this.pollWidgetData(ApiEntity.COUNT, { viewName }, component);
  }

  getChartData(chartType: string, component: OnDestroy) {
    return this.pollWidgetData(ApiEntity.CHART_DATA, { chartType }, component, AppConstants.UserSettings.LongTimeInterval);
  }

  getProcessStats(component: OnDestroy) {
    return this.pollWidgetData(ApiEntity.PROCESS_STATS, {}, component, AppConstants.UserSettings.LongTimeInterval);
  }

  private pollWidgetData(entity: ApiEntity, args: any, component: OnDestroy, interval: number = AppConstants.UserSettings.RealTimeInterval) {
    const observable = new Subject<any>();
    let lock = false;
    timer(0, interval)
      .pipe(
        untilDestroyed(component)
      )
      .subscribe(() => {
        if (!AppConstants.RealTimeEnabled || lock) {
          return;
        }

        lock = true;
        this.apiService.get(entity, args).subscribe(data => {
          observable.next(data);
          lock = false;
        });
      });
    return observable.pipe(untilDestroyed(component));
  }


}
