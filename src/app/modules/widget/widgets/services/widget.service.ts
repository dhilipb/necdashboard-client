import { Injectable } from '@angular/core';
import { ApiEntity, DashboardWidget } from '@core/model';
import { ApiService, LocalCacheService, StoreService } from '@core/services';
import { GridsterItemComponentInterface } from 'angular-gridster2';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class WidgetService {

  public onResizeWidget: BehaviorSubject<GridsterItemComponentInterface> = new BehaviorSubject(null);

  constructor(
    private storeService: StoreService,
    private localCacheService: LocalCacheService,
    private apiService: ApiService
  ) { }

  updateWidget(dashboardName: string, widget: DashboardWidget) {
    this.apiService.edit(ApiEntity.WIDGET, {
      x: widget.x,
      y: widget.y,
      rows: widget.rows,
      cols: widget.cols,
      type: widget.type,
      widgetId: widget.widgetId
    });

    this.storeService.dashboardItems[dashboardName] = this.storeService.dashboardItems[dashboardName].filter(w => w.widgetId !== widget.widgetId);
    this.storeService.dashboardItems[dashboardName].push(widget);
  }

  deleteWidget(widget: DashboardWidget) {
    this.apiService.delete(ApiEntity.WIDGET, {
      widgetId: widget.widgetId
    }).subscribe(result => {
      // Remove from store
      Object.entries(this.storeService.dashboardItems).forEach(([dashboardName, widgets]) => {
        if (widgets.find(w => w.widgetId === widget.widgetId)) {
          this.storeService.dashboardItems[dashboardName] = this.storeService.dashboardItems[dashboardName].filter(w => w.widgetId !== widget.widgetId);
        }
      });
    });
  }

}
