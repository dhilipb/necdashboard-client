import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveCamerasWidgetComponent } from './live-cameras-widget.component';

/* tslint:disable:no-unused-variable */
describe('LiveCamerasWidgetComponent', () => {
  let component: LiveCamerasWidgetComponent;
  let fixture: ComponentFixture<LiveCamerasWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LiveCamerasWidgetComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveCamerasWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
