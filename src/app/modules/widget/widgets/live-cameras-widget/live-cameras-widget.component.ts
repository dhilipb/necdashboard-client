import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AppConstants } from '@core/config';
import { WidgetComponent, WidgetComponentOptions } from '@core/model';
import { CameraService } from '@core/services';
import { untilDestroyed } from '@core/until-destroy';
import { get } from 'lodash';

import { WidgetSettingsService } from '../../options/services/widget-settings.service';

@Component({
  selector: 'widget-cameras',
  templateUrl: './live-cameras-widget.component.html',
  styleUrls: ['./live-cameras-widget.component.scss']
})
export class LiveCamerasWidgetComponent implements OnInit, WidgetComponent, OnDestroy {

  public cameras: any[] = [];
  public camerasCols: number = 4;
  public cameraHeight: number;

  constructor(
    @Inject('widgetId') private widgetId: string,
    private cameraService: CameraService,
    private sanitizer: DomSanitizer,
    private widgetSettingsService: WidgetSettingsService
  ) {
  }

  ngOnDestroy() { }

  ngOnInit() {
    const totalCameras = this.widgetSettingsService.retrieveKey(this.widgetId, 'totalCameras');
    let cameraCols = this.widgetSettingsService.retrieveKey(this.widgetId, 'numberOfCamerasPerRow');
    if (totalCameras < cameraCols) {
      cameraCols = totalCameras;
    }
    this.camerasCols = Math.floor(12 / parseInt(cameraCols, 10));
    this.cameraHeight = this.widgetSettingsService.retrieveKey(this.widgetId, 'cameraHeight');

    const liveViewUrl = this.widgetSettingsService.retrieveKey(this.widgetId, 'liveViewUrl');
    this.cameraService.getCameras()
      .pipe(untilDestroyed(this)).subscribe(data => {
        const cameras = get(data, 'response', []).slice(0, totalCameras);
        cameras.forEach(camera => {
          const cameraUrl = AppConstants.UserSettings.NecUrl + liveViewUrl + camera.CameraID;
          const cameraUrlSanitized = this.sanitizer.bypassSecurityTrustResourceUrl(cameraUrl);
          camera.url = cameraUrlSanitized;
        });
        this.cameras = cameras;
      });
  }

  public getOptions(): WidgetComponentOptions {
    return {
      widgetTitle: { value: 'Live Cameras' },
      totalCameras: {
        title: 'Total Cameras',
        value: 99,
        type: 'number'
      },
      numberOfCamerasPerRow: {
        title: 'Cameras per row',
        value: 4,
        type: 'number'
      },
      cameraHeight: {
        title: 'Camera Height',
        value: 300
      },
      liveViewUrl: {
        title: 'Live View URL',
        value: '/MediaPlayerWidget/PopupLiveView?cameraId='
      }
    };
  }

}
