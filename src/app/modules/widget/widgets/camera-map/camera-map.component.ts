import { ChangeDetectorRef, Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { WidgetComponent, WidgetComponentOptions } from '@core/model';
import { CameraService } from '@core/services';
import { untilDestroyed } from '@core/until-destroy';
import { CRS, featureGroup, icon, IconOptions, ImageOverlay, imageOverlay, LatLng, latLng, LatLngBounds, Map, MapOptions, Marker, marker, tileLayer } from 'leaflet';
import { get } from 'lodash';
import { first } from 'rxjs/operators';

import { WidgetSettingsService } from '../../options/services/widget-settings.service';
import { WidgetOptionMapFormData } from '../../options/widget-options-dialog';
import { WidgetService } from '../services';

@Component({
  selector: 'dash-camera-map',
  templateUrl: './camera-map.component.html',
  styleUrls: ['./camera-map.component.scss']
})
export class CameraMapComponent implements OnInit, OnDestroy, WidgetComponent {

  public cameras: string[] = [];

  private map: Map;
  public options: MapOptions = {};
  public markerLayers: Marker[] = [];
  private formData: WidgetOptionMapFormData;

  private customMapLayer: ImageOverlay;

  @ViewChild('mapContainer', { static: false }) mapContainer: ElementRef;

  private readonly ICON_OPTIONS: IconOptions = {
    iconSize: [30, 30],
    iconAnchor: [0, 15],
    iconUrl: 'assets/cctv.png',
  };

  constructor(
    @Inject('widgetId') private widgetId: string,
    private changeDetectorRef: ChangeDetectorRef,
    private widgetService: WidgetService,
    private widgetSettingsService: WidgetSettingsService,
    private cameraService: CameraService
  ) {
  }

  ngOnInit() {

    // Get all the cameras for tooltip
    this.cameraService.getCameras()
      .pipe(first())
      .subscribe(output => {
        this.cameras = get(output, 'response');
      });


    // Refresh on resize
    this.widgetService.onResizeWidget.pipe(untilDestroyed(this)).subscribe(itemComponent => {
      if (itemComponent && itemComponent.item.widgetId === this.widgetId && this.map) {
        this.onContainerResize();
      }
    });


    // Initialise map
    const mapDataRaw = this.widgetSettingsService.retrieveKey(this.widgetId, 'mapData');
    try {
      this.formData = JSON.parse(mapDataRaw) as WidgetOptionMapFormData;
    } catch (e) {
      console.error('Could not parse', e);
    }
    this.initMap();
  }

  /**
   * Initialise map based on type
   */
  private initMap(): void {
    if (this.formData.type === 'custom') {
      this.initCustomMap();
    } else {
      this.initStreetMap();
    }
  }

  private initStreetMap(): void {
    this.options = {
      zoomControl: true,
      layers: [
        tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {}),
      ],
      zoom: 13,
      center: latLng(25.1561725, 55.2771853),
      attributionControl: false
    };
  }

  private initCustomMap(): void {
    this.options = {
      zoomSnap: 0.1,
      zoomControl: false,
      minZoom: 1,
      maxZoom: 4,
      center: [0, 0],
      zoom: 1,
      crs: CRS.Simple,
      attributionControl: false
    };

    if (!this.formData.map) {
      this.formData.map = {
        width: 979,
        height: 599,
        url: 'assets/map.jpg'
      };
    }
  }

  private onContainerResize() {

    // Set height of container
    this.changeDetectorRef.detectChanges();
    const mapContainerHeight = this.mapContainer.nativeElement.scrollWidth * this.formData.map.height / this.formData.map.width;
    this.mapContainer.nativeElement.style.height = Math.round(mapContainerHeight) + 'px';

    this.map.invalidateSize();

    if (this.formData.type === 'custom') {
      // Calculate the edges of the image and fit the image
      const southWest = this.map.unproject([0, this.formData.map.height], this.map.getMaxZoom() - 1);
      const northEast = this.map.unproject([this.formData.map.width, 0], this.map.getMaxZoom() - 1);
      const bounds = new LatLngBounds(southWest, northEast);

      if (this.customMapLayer) {
        this.map.removeLayer(this.customMapLayer);
      }

      this.customMapLayer = imageOverlay(this.formData.map.url, bounds);
      this.customMapLayer.addTo(this.map);

      const wantedZoom = this.map.getBoundsZoom(bounds, true);
      this.map.setMinZoom(wantedZoom);
      this.map.setView(bounds.getCenter(), wantedZoom);

      this.map.setMaxBounds(bounds);
    } else {
      const group = featureGroup(this.markerLayers);
      this.map.fitBounds(group.getBounds());
    }
  }


  onMapReady(map: Map) {
    this.map = map;

    for (const markerItem of this.formData.markers) {
      this.addMarker(markerItem.camera, markerItem.lat, markerItem.lng);
    }

    this.onContainerResize();
  }

  addMarker(cameraId: string, lat?: number, lng?: number) {
    const markerLocation = lat && lng ? { lat, lng } as LatLng : this.map.getCenter();
    const cameraDetails = this.cameras.find(cam => cameraId === cam['CameraID']);
    const cameraName = cameraDetails ? cameraDetails['Name'] : '';

    const markerPoint = marker(markerLocation, {
      draggable: false,
      icon: icon(this.ICON_OPTIONS),
      title: cameraId,
      alt: cameraName
    });

    markerPoint.bindTooltip(cameraName, {
      sticky: true
    }).openTooltip();

    this.markerLayers.push(markerPoint);
  }


  public getOptions(): WidgetComponentOptions {
    return {
      widgetTitle: { value: 'Camera Map' },
      dataView: {
        title: 'Database View',
        type: 'db-views',
        value: 'LastMatchWidgetViewIS'
      },
      mapData: {
        title: 'Locations',
        type: 'maps',
        value: ''
      }
    };
  }
  ngOnDestroy() { }

}

