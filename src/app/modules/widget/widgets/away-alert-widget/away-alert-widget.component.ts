import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { AppConstants } from '@core/config';
import { ApiEntity, NecVisitor, WidgetComponent, WidgetComponentOptions } from '@core/model';
import { ApiService } from '@core/services';
import { untilDestroyed } from '@core/until-destroy';
import { get, isEqual, uniq } from 'lodash';

import { WidgetSettingsService } from '../../options/services/widget-settings.service';
import { WidgetDataService } from '../services';
import { WidgetService } from '../services/widget.service';

@Component({
  selector: 'widget-away-alert',
  templateUrl: './away-alert-widget.component.html',
  styleUrls: ['./away-alert-widget.component.scss']
})
export class AwayAlertWidgetComponent implements OnInit, OnDestroy, WidgetComponent {

  public users: NecVisitor[];
  public shouldShowPicture: boolean = true;
  public loading: boolean = true;

  public containerHeight: number = 100;

  private hiddenUsers: string[] = [];
  private hiddenUserSettingId: string;

  constructor(
    @Inject('widgetId') private widgetId: string,
    private widgetDataService: WidgetDataService,
    private widgetService: WidgetService,
    private widgetSettingsService: WidgetSettingsService,
    private apiService: ApiService
  ) { }

  ngOnInit() {
    const dataView = this.widgetSettingsService.retrieveKey(this.widgetId, 'dataView');
    this.updateWidgetData(dataView);
    this.shouldShowPicture = this.widgetSettingsService.retrieveKey(this.widgetId, 'showPicture');

    // Get hidden users
    this.updateHiddenUsers();


    // Set container height on resize
    this.widgetService.onResizeWidget.pipe(untilDestroyed(this)).subscribe(itemComponent => {
      if (itemComponent && itemComponent.item.widgetId === this.widgetId) {
        this.containerHeight = Math.floor(itemComponent.height) - 50;
      }
    });
  }

  private updateHiddenUsers() {
    this.apiService.get(ApiEntity.WIDGET_DATA, {
      widgetid: this.widgetId
    }).subscribe(data => {
      const widgetDataItems = get(data, 'response');
      const hiddenUserSettings = widgetDataItems.filter(setting => setting.key === 'hidden-users-away-alert');
      if (hiddenUserSettings) {
        this.hiddenUsers = hiddenUserSettings.map(setting => setting.value);
      }
      console.log(this.hiddenUsers);
    });
  }

  private updateWidgetData(dataView: any) {
    this.widgetDataService.getData(dataView, this).subscribe(data => {
      this.loading = false;
      const visitors: NecVisitor[] = get(data, 'response');
      const noOfVisitors = this.widgetSettingsService.retrieveKey(this.widgetId, 'noOfVisitors');

      const users = visitors.filter(visitor => !this.hiddenUsers.includes(visitor.MatchResultID)).slice(0, noOfVisitors);
      users.forEach(user => {
        if (user && user.Colour) {
          user.HexColour = '#' + parseInt(user.Colour, 10).toString(16);
        }
      });
      if (!isEqual(users, this.users)) {
        this.users = users;
      }
    });
  }

  ngOnDestroy() { }

  public getOptions(): WidgetComponentOptions {
    return {
      widgetTitle: { value: 'Recent Visitors' },
      showPicture: {
        title: 'Show Picture?',
        value: true
      },
      pictureUrl: {
        title: 'Picture URL',
        value: AppConstants.UserSettings.NecUrl + '/Image/GetSubjectFaceThumbnail?id='
      },
      noOfVisitors: {
        title: 'No. of visitors',
        value: 5
      },
      dataView: {
        title: 'Database View',
        type: 'db-views',
        value: 'LastMatchWidgetViewIS'
      },
    };
  }

  getImage(user: any) {
    return this.widgetSettingsService.retrieveKey(this.widgetId, 'pictureUrl', '') + user.SubjectPhotoId;
  }

  hideUser(user: NecVisitor): void {
    if (!confirm('Are you sure you wish to close this entry?')) {
      return;
    }

    this.hiddenUsers.push(user.MatchResultID);
    this.hiddenUsers = uniq(this.hiddenUsers);

    this.apiService.create(ApiEntity.WIDGET_DATA, {
      widgetid: this.widgetId,
      key: 'hidden-users-away-alert',
      value: user.MatchResultID
    });

  }

}
