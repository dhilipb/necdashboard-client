import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountWidgetComponent } from './count-widget.component';

/* tslint:disable:no-unused-variable */
describe('CountWidgetComponent', () => {
  let component: CountWidgetComponent;
  let fixture: ComponentFixture<CountWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CountWidgetComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
