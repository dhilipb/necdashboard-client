import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { WidgetComponent, WidgetComponentOptions } from '@core/model';
import { get } from 'lodash';

import { WidgetSettingsService } from '../../options/services/widget-settings.service';
import { WidgetDataService } from '../services';

@Component({
  selector: 'widget-count',
  templateUrl: './count-widget.component.html',
  styleUrls: ['./count-widget.component.scss']
})
export class CountWidgetComponent implements OnInit, OnDestroy, WidgetComponent {

  public loading: boolean = true;
  public count: number = 0;

  constructor(
    @Inject('widgetId') private widgetId: string,
    private widgetSettingsService: WidgetSettingsService,
    private widgetDataService: WidgetDataService
  ) { }

  ngOnInit() {
    const dataColumn = this.widgetSettingsService.retrieveKey(this.widgetId, 'dataColumn');
    const dataView = this.widgetSettingsService.retrieveKey(this.widgetId, 'dataView');
    this.widgetDataService.getCountData(dataView, this).subscribe(data => {
      this.count = parseInt(get(data, 'response[0].' + dataColumn) || get(data, 'response[0].Nos'), 10);
      this.loading = false;
    });

  }

  ngOnDestroy() { }

  public getOptions(): WidgetComponentOptions {
    return {
      widgetTitle: { value: 'Number of Visitors' },
      dataView: {
        title: 'Database View',
        type: 'db-views',
        value: 'ISMatchCount'
      },
      dataColumn: {
        title: 'Data column',
        value: 'Count'
      }
    };
  }

}
