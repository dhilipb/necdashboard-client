import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { ConfettiModule } from '@shared/confetti';
import { HighchartsChartModule } from 'highcharts-angular';
import { Ng2OdometerModule } from 'ng2-odometer';

import { SharedModule } from '../../shared/shared.module';
import { VisitorModule } from '../visitor/visitor.module';
import { WidgetOptionInputListComponent, WidgetOptionMapsComponent } from './options/widget-options-dialog';
import { WidgetOptionsDialogComponent } from './options/widget-options-dialog/widget-options-dialog.component';
import { WidgetComponentsList } from './widget-components';
import { WidgetContainerComponent } from './widgets/widget-container/widget-container.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    Ng2OdometerModule.forRoot(),
    ConfettiModule,
    HighchartsChartModule,
    VisitorModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    LeafletModule.forRoot()
  ],
  declarations: [
    ...WidgetComponentsList,
    WidgetContainerComponent,
    WidgetOptionsDialogComponent,
    WidgetOptionInputListComponent,
    WidgetOptionMapsComponent
  ],
  exports: [
    ...WidgetComponentsList,
    WidgetContainerComponent,
    WidgetOptionsDialogComponent
  ]
})
export class WidgetModule {

  constructor() {

  }

}
