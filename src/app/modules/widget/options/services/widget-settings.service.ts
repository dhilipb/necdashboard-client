import { Injectable } from '@angular/core';
import { ApiEntity, WidgetComponentOptions } from '@core/model';
import { ApiService, StoreService } from '@core/services';
import { get } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class WidgetSettingsService {

  constructor(
    private storeService: StoreService,
    private apiService: ApiService
  ) {

  }

  public save(widgetId: string, widgetOptions: WidgetComponentOptions) {
    const widget = this.storeService.getWidget(widgetId);
    Object.entries(widgetOptions).forEach(([key, value]) => {
      this.apiService.create(ApiEntity.WIDGET_SETTINGS, { widgetId, key, value: value.value });
      widget.options[key] = value;
    });
    console.log('before', this.storeService.getWidget(widgetId));

  }

  public retrieve(widgetId: string): WidgetComponentOptions {
    const widget = this.storeService.getWidget(widgetId);
    return widget.options;
  }

  public retrieveKey(widgetId: string, key: string, defaultValue?: any): any {
    const value = get(this.retrieve(widgetId), key + '.value', defaultValue);
    if (value === 'true' || value === 'false') {
      return value === 'true';
    } else {
      return value;
    }
  }

}
