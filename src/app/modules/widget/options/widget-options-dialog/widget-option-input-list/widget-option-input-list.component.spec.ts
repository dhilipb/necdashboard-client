import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetOptionInputListComponent } from './widget-option-input-list.component';

/* tslint:disable:no-unused-variable */
describe('WidgetOptionInputListComponent', () => {
  let component: WidgetOptionInputListComponent;
  let fixture: ComponentFixture<WidgetOptionInputListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WidgetOptionInputListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetOptionInputListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
