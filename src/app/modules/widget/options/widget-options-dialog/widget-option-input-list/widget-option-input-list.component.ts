import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'widget-option-input-list',
  templateUrl: './widget-option-input-list.component.html',
  styleUrls: ['./widget-option-input-list.component.scss']
})
export class WidgetOptionInputListComponent implements OnInit {

  @Input() widgetOptionsForm: FormGroup;
  @Input() widgetOption: any;

  constructor() { }

  ngOnInit() {
  }

  add(widgetOption: any) {
    (this.widgetOptionsForm.get(widgetOption.key) as FormArray).push(new FormControl());
  }
  remove(widgetOption: any, i: number) {
    (this.widgetOptionsForm.get(widgetOption.key) as FormArray).removeAt(i);
  }

}
