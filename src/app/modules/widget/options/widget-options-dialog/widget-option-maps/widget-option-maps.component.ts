import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CameraService } from '@core/services';
import { CRS, featureGroup, icon, IconOptions, imageOverlay, LatLng, latLng, LatLngBounds, Map, MapOptions, Marker, marker, tileLayer } from 'leaflet';
import { get, remove } from 'lodash';
import { first } from 'rxjs/operators';

export interface WidgetOptionMapFormData {
  type: 'custom' | 'street';
  map?: {
    url: string,
    width: number,
    height: number
  };
  markers: {
    camera: string,
    lat: number,
    lng: number
  }[];
}

@Component({
  selector: 'widget-option-maps',
  templateUrl: './widget-option-maps.component.html',
  styleUrls: ['./widget-option-maps.component.scss']
})
export class WidgetOptionMapsComponent implements OnInit {

  @Input() widgetOptionsForm: FormGroup;
  @Input() widgetOption: any;

  private map: Map;
  public options: MapOptions = {};
  public markerLayers: Marker[] = [];

  public cameras: string[] = [];

  private formData: WidgetOptionMapFormData;

  private readonly ICON_OPTIONS: IconOptions = {
    iconSize: [30, 30],
    iconAnchor: [0, 15],
    iconUrl: 'assets/cctv.png',
  };

  constructor(
    private cameraService: CameraService
  ) {
  }

  ngOnInit(): void {
    this.formData = {
      type: 'custom',
      markers: []
    };

    this.cameraService.getCameras()
      .pipe(first())
      .subscribe(output => {
        this.cameras = get(output, 'response');
      });

    const formValue = this.widgetOptionsForm.get(this.widgetOption.key).value;
    if (formValue) {
      try {
        this.formData = JSON.parse(formValue) as WidgetOptionMapFormData;
      } catch (e) {
        console.error('Could not parse', e);
      }
    }

    this.initMap();
  }

  onMapReady(map: Map) {
    this.map = map;

    for (const markerItem of this.formData.markers) {
      this.addMarker(markerItem.camera, markerItem.lat, markerItem.lng);
    }

    if (this.formData.type === 'custom') {
      // calculate the edges of the image, in coordinate space
      const southWest = this.map.unproject([0, this.formData.map.height], this.map.getMaxZoom() - 1);
      const northEast = this.map.unproject([this.formData.map.width, 0], this.map.getMaxZoom() - 1);
      const bounds = new LatLngBounds(southWest, northEast);
      imageOverlay(this.formData.map.url, bounds).addTo(this.map);

      this.map.setMaxBounds(bounds);
    } else {
      const group = featureGroup(this.markerLayers);
      this.map.fitBounds(group.getBounds());
    }
  }

  public initMap(): void {
    this.options = null;
    setTimeout(() => {
      if (this.formData.type === 'custom') {
        this.initCustomMap();
      } else {
        this.initStreetMap();
      }
    }, 100);
  }

  private initStreetMap(): void {
    this.options = {
      zoomControl: true,
      layers: [
        tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {}),
      ],
      zoom: 13,
      center: latLng(25.1561725, 55.2771853),
      attributionControl: false
    };
  }

  private initCustomMap(): void {
    this.options = {
      zoomControl: false,
      minZoom: 2,
      maxZoom: 4,
      center: [0, 0],
      zoom: 2,
      crs: CRS.Simple,
      attributionControl: false
    };

    if (!this.formData.map) {
      this.formData.map = {
        width: 979,
        height: 599,
        url: 'assets/map.jpg'
      };
    }

  }

  addMarker(cameraId: string, lat?: number, lng?: number) {
    const markerLocation = lat && lng ? { lat, lng } as LatLng : this.map.getCenter();
    const autoPan = this.formData.type === 'street';
    const cameraDetails = this.cameras.find(cam => cameraId === cam['CameraID']);
    const cameraName = cameraDetails ? cameraDetails['Name'] : '';

    const markerPoint = marker(markerLocation, {
      draggable: true,
      icon: icon(this.ICON_OPTIONS),
      autoPan,
      title: cameraId,
      alt: cameraName
    });

    markerPoint.bindTooltip(cameraName, {
      sticky: true
    }).openTooltip();

    markerPoint.on('dragend', event => {
      this.updateFormData();
    });
    markerPoint.on('dblclick', event => {
      remove(this.markerLayers, layer => layer === event.target);
      this.updateFormData();
    });
    this.markerLayers.push(markerPoint);
    this.updateFormData();
  }

  private updateFormData() {
    this.formData.markers = this.markerLayers.map(layer => {
      return { ...layer.getLatLng(), camera: layer.options.title };
    });
    this.widgetOptionsForm.get(this.widgetOption.key).setValue(JSON.stringify(this.formData));
    console.log('formData', this.widgetOptionsForm.get(this.widgetOption.key).value);
  }



}
