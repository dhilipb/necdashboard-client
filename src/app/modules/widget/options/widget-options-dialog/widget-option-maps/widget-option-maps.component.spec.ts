import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetOptionMapsComponent } from './widget-option-maps.component';

/* tslint:disable:no-unused-variable */
describe('WidgetOptionMapsComponent', () => {
  let component: WidgetOptionMapsComponent;
  let fixture: ComponentFixture<WidgetOptionMapsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WidgetOptionMapsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetOptionMapsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
