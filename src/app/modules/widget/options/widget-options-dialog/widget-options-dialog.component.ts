import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { WidgetComponentOptions } from '@core/model';
import { DialogConfig, DialogRef } from '@shared/dialog';
import { GridsterItem } from 'angular-gridster2';
import { get, has, isArray } from 'lodash';

import { WidgetDataService } from '../../widgets/services';
import { WidgetSettingsService } from '../services/widget-settings.service';

@Component({
  selector: 'dash-widget-options-dialog',
  templateUrl: './widget-options-dialog.component.html',
  styleUrls: ['./widget-options-dialog.component.scss']
})
export class WidgetOptionsDialogComponent implements OnInit {

  public item: GridsterItem;
  public widgetOptionsForm: FormGroup = new FormGroup({});
  public widgetOptions: WidgetComponentOptions = {} as WidgetComponentOptions;
  public databaseViews: Array<any>;

  public keepOriginalOrder = (a, b) => a.key;

  constructor(
    private config: DialogConfig,
    private dialogRef: DialogRef,
    private widgetSettingsService: WidgetSettingsService,
    private widgetDataService: WidgetDataService
  ) {
    this.item = get(config, 'data.item');
    this.databaseViews = this.widgetDataService.databaseViews;
  }

  ngOnInit() {
    const componentOptions: any = has(this.item, 'component.prototype') ? get(this.item, 'component.prototype').getOptions() : {};
    if ('widgetTitle' in componentOptions) {
      this.initForm(componentOptions);
    } else {
      console.error('No widget options provided', typeof componentOptions, componentOptions);
    }
  }

  initForm(widgetOptions: WidgetComponentOptions) {
    const widgetId = this.item.widgetId;
    Object.entries(widgetOptions).forEach(([widgetKey, widgetValue]) => {

      let savedValue = this.widgetSettingsService.retrieveKey(widgetId, widgetKey, widgetValue);
      if (widgetValue['type'] === 'list-input') {
        savedValue = JSON.parse(savedValue as string);
        widgetValue['value'] = JSON.parse(widgetValue['value'] as string);
      }

      if (isArray(savedValue)) {
        this.widgetOptionsForm.registerControl(widgetKey, new FormArray(savedValue.map(value => new FormControl(value)), Validators.required));
      } else {
        this.widgetOptionsForm.registerControl(widgetKey, new FormControl(savedValue, Validators.required));
      }

      // Add title to options
      if (widgetKey === 'widgetTitle') {
        widgetValue = { title: 'Widget Title', value: widgetValue } as any;
      }

      // Set the type if it doesn't exist
      widgetValue['type'] = widgetValue['type'] || typeof widgetValue['value'];


      this.widgetOptions[widgetKey] = widgetValue;
    });

  }

  onSubmit() {
    // Update widgetOptions with value from form
    const controls = this.widgetOptionsForm.controls;
    Object.keys(controls).forEach(control => {
      const value = controls[control].value;
      if (isArray(value)) {
        this.widgetOptions[control].value = JSON.stringify(controls[control].value);
      } else {
        this.widgetOptions[control].value = controls[control].value;
      }
    });

    this.widgetSettingsService.save(this.item.widgetId, this.widgetOptions);
    this.dialogRef.close('submit');
  }

  reset() {
    this.widgetOptionsForm.reset();
  }


  log(w: any) {
    console.log(w);
  }

}
