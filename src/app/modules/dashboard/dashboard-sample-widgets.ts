import { DashboardWidget } from '@core/model';

import { CameraMapComponent, ChartWidgetComponent, CountWidgetComponent, LastHitWidgetComponent, LiveCamerasWidgetComponent, LogosWidgetComponent, RecentVisitorsWidgetComponent, TableDataWidgetComponent } from '../widget/widgets';

export const HomeSampleWidgets: Array<DashboardWidget> = [
    {
        cols: 30, rows: 16, x: 0, y: 0,
        widgetId: 'LiveCamerasWidgetComponent_1',
        component: LiveCamerasWidgetComponent
    },
    {
        cols: 19, rows: 24, x: 0, y: 16,
        widgetId: 'CameraMapComponent_2',
        component: CameraMapComponent
    },
    {
        cols: 12, rows: 17, x: 30, y: 0,
        widgetId: 'LastHitWidgetComponent_3',
        component: LastHitWidgetComponent
    },
    {
        cols: 11, rows: 24, x: 19, y: 16,
        widgetId: 'RecentVisitorsWidgetComponent_4',
        component: RecentVisitorsWidgetComponent
    },
    {
        cols: 6, rows: 6, x: 30, y: 17,
        widgetId: 'CountWidgetComponent_5',
        component: CountWidgetComponent
    },
    {
        cols: 6, rows: 6, x: 36, y: 17,
        widgetId: 'LogosWidgetComponent_6',
        component: LogosWidgetComponent,
        options: {
            widgetTitle: {
                value: ''
            }
        }
    },
    {
        cols: 12, rows: 17, x: 30, y: 23,
        widgetId: 'ChartWidgetComponent_7',
        component: ChartWidgetComponent
    }
];
export const AnalyticsSampleWidgets: Array<DashboardWidget> = [
    {
        cols: 7,
        rows: 6,
        x: 0,
        y: 0,
        widgetId: 'CountWidgetComponent_8',
        component: CountWidgetComponent,
        options: {
            widgetTitle: {
                value: 'Visitors'
            },
            dataView: {
                value: 'ISMatchCount'
            }
        }
    },
    {
        cols: 7,
        rows: 6,
        x: 35,
        y: 0,
        widgetId: 'CountWidgetComponent_9',
        component: CountWidgetComponent,
        options: {
            widgetTitle: {
                value: 'Cameras'
            },
            dataView: {
                value: 'ISCameraNos'
            }
        }
    },
    {
        cols: 7,
        rows: 6,
        x: 7,
        y: 0,
        widgetId: 'CountWidgetComponent_10',
        component: CountWidgetComponent,
        options: {
            widgetTitle: {
                value: 'Locations'
            },
            dataView: {
                value: 'ISLocnNos'
            }
        }
    },
    {
        cols: 7,
        rows: 6,
        x: 28,
        y: 0,
        widgetId: 'CountWidgetComponent_11',
        component: CountWidgetComponent,
        options: {
            widgetTitle: {
                value: 'Watchlists'
            },
            dataView: {
                value: 'ISWatchListNos'
            }
        }
    },
    {
        cols: 7,
        rows: 6,
        x: 14,
        y: 0,
        widgetId: 'CountWidgetComponent_12',
        component: CountWidgetComponent,
        options: {
            widgetTitle: {
                value: 'Males'
            },
            dataView: {
                value: 'ISGenderCountMale'
            }
        }
    },
    {
        cols: 7,
        rows: 6,
        x: 21,
        y: 0,
        widgetId: 'CountWidgetComponent_13',
        component: CountWidgetComponent,
        options: {
            widgetTitle: {
                value: 'Females'
            },
            dataView: {
                value: 'ISGenderCountFemale'
            }
        }
    },
    {
        cols: 14,
        rows: 14,
        x: 14,
        y: 6,
        widgetId: 'ChartWidgetComponent_14',
        component: ChartWidgetComponent,
        options: {
            widgetTitle: {
                value: 'Hourly Visitors'
            },
            dataView: {
                value: 'ISHourCount'
            }
        }
    },
    {
        cols: 14,
        rows: 14,
        x: 0,
        y: 6,
        widgetId: 'ChartWidgetComponent_15',
        component: ChartWidgetComponent,
        options: {
            widgetTitle: {
                value: 'Cameras'
            },
            dataView: {
                value: 'ISCameraCount'
            }
        }
    },
    {
        cols: 14,
        rows: 14,
        x: 0,
        y: 20,
        widgetId: 'ChartWidgetComponent_16',
        component: ChartWidgetComponent,
        options: {
            widgetTitle: {
                value: 'Watchlists'
            },
            dataView: {
                value: 'ISWatchListCount'
            },
            chartType: {
                value: 'pie'
            }
        }
    },
    {
        cols: 14,
        rows: 14,
        x: 14,
        y: 20,
        widgetId: 'ChartWidgetComponent_17',
        component: ChartWidgetComponent,
        options: {
            widgetTitle: {
                value: 'Date'
            },
            dataView: {
                value: 'ISDateCount'
            },
            chartColour: {
                value: 'orange'
            }
        }
    },
    {
        cols: 14,
        rows: 14,
        x: 28,
        y: 20,
        widgetId: 'ChartWidgetComponent_18',
        component: ChartWidgetComponent,
        options: {
            widgetTitle: {
                value: 'Top Repeated'
            },
            dataView: {
                value: 'ISRepeatVisitors'
            },
            chartType: {
                value: 'pie'
            }
        }
    },
    {
        cols: 14,
        rows: 14,
        x: 28,
        y: 6,
        widgetId: 'ChartWidgetComponent_19',
        component: ChartWidgetComponent,
        options: {
            widgetTitle: {
                value: 'Score'
            },
            dataView: {
                value: 'ISScoreCount'
            },
            chartType: {
                value: 'bar'
            },
            chartColour: {
                value: 'blue'
            }
        }
    },
    {
        cols: 42,
        rows: 6,
        x: 0,
        y: 34,
        widgetId: 'TableDataWidgetComponent_20',
        component: TableDataWidgetComponent,
        options: {
            widgetTitle: { value: '' }
        }
    }
];
