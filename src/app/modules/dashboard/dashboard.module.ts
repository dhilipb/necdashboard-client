import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreModule } from '@core/core.module';
import { DialogComponent, DialogModule } from '@shared/dialog';
import { SharedModule } from '@shared/shared.module';
import { GridsterModule } from 'angular-gridster2';

import { AddWidgetModule } from '../widget/add-widget/add-widget.module';
import { WidgetOptionsDialogComponent } from '../widget/options/widget-options-dialog/widget-options-dialog.component';
import { WidgetComponents } from '../widget/widget-components';
import { WidgetModule } from '../widget/widget.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';

const routes = [{ path: 'dashboard', component: DashboardComponent }];
@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    GridsterModule,
    SharedModule,
    CoreModule,
    DashboardRoutingModule,
    HttpClientModule,
    WidgetModule,
    DialogModule,
    AddWidgetModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [
    ...Object.values(WidgetComponents),
    DialogComponent,
    WidgetOptionsDialogComponent
  ]
})
export class DashboardModule {
  constructor() {

  }
}
