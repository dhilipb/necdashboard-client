import { Component, Injector, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConstants } from '@core/config';
import { DashboardWidget } from '@core/model';
import { LocalCacheService, StoreService } from '@core/services';
import { DialogService } from '@shared/dialog';
import { CompactType, GridsterConfig, GridsterItem, GridsterItemComponentInterface, GridType } from 'angular-gridster2';
import { get } from 'lodash';
import { first } from 'rxjs/operators';

import { WelcomeVisitorDialogComponent } from '../visitor';
import { VisitorInfoDialogComponent } from '../visitor/visitor-info-dialog/visitor-info-dialog.component';
import { AddWidgetDialogComponent } from '../widget/add-widget/add-widget-dialog/add-widget-dialog.component';
import { WidgetOptionsDialogComponent } from '../widget/options/widget-options-dialog/widget-options-dialog.component';
import { WidgetDataService } from '../widget/widgets/services';
import { WidgetService } from '../widget/widgets/services/widget.service';
import { DashboardService } from './services/dashboard.service';

@Component({
  selector: 'dashboard-main',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  public loading: boolean = true;
  public dashboardName: string;
  public options: GridsterConfig;
  private gridsterItemComponents: { [widgetId: string]: GridsterItemComponentInterface } = {};

  @ViewChild(WidgetOptionsDialogComponent, { static: false }) optionsDialogComponent: WidgetOptionsDialogComponent;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public dashboardService: DashboardService,
    private widgetService: WidgetService,
    private widgetDataService: WidgetDataService,
    private dialogService: DialogService,
    private storeService: StoreService,
    private localCacheService: LocalCacheService,
    private injector: Injector
  ) { }

  ngOnInit() {

    this.route.queryParams.subscribe(queryParams => {
      const dashboardAction = get(queryParams, 'action');
      if (dashboardAction) {
        this.performDashboardAction(dashboardAction);
      }
    });

    this.dashboardName = this.route.snapshot.params['id'];
    this.route.params.subscribe(params => {
      this.dashboardName = params['id'];
    });

    this.initDashboardOptions();

    this.dashboardService.retrieveDashboards(this.injector).subscribe(dashboards => {
      this.loading = false;

      this.widgetDataService.init();
      this.initVisitorDialog();
    });
  }


  private initDashboardOptions() {
    this.options = {
      gridType: GridType.Fit,
      compactType: CompactType.None,
      minCols: 42,
      maxCols: 42,
      minRows: 40,
      maxRows: 40,
      pushItems: true,
      draggable: {
        enabled: !AppConstants.UserSettings.Locked
      },
      resizable: {
        enabled: !AppConstants.UserSettings.Locked
      },
      displayGrid: 'onDrag&Resize',
      itemResizeCallback: (item: GridsterItem, itemComponent: GridsterItemComponentInterface) => {
        this.widgetService.onResizeWidget.next(itemComponent);
      },
      itemChangeCallback: (item: GridsterItem, itemComponent: GridsterItemComponentInterface) => {
        this.widgetService.updateWidget(this.dashboardName, item as DashboardWidget);
      },
      itemInitCallback: (item: GridsterItem, itemComponent: GridsterItemComponentInterface) => {
        this.gridsterItemComponents[item.widgetId] = itemComponent;
      }
    };
  }

  ngOnDestroy() { }


  public onWidgetActionClick(actionEvent: string, item: GridsterItem) {
    if (actionEvent === 'delete') {
      this.widgetService.deleteWidget(item as DashboardWidget);
    } else if (actionEvent === 'edit') {
      const dialogRef = this.dialogService.open(WidgetOptionsDialogComponent, {
        data: {
          title: 'Options',
          titleBorder: true,
          item
        }
      });
      dialogRef.afterClosed.pipe(first()).subscribe(result => {
        // Update component
        if (result === 'submit') {
          const component = item.component;
          item.component = null;
          setTimeout(() => {
            item.component = component;
            this.widgetService.onResizeWidget.next(this.gridsterItemComponents[item.widgetId]);
          });
        }
      });
    }
  }


  private performDashboardAction(dashboardAction: any) {
    if (dashboardAction === 'add') {
      const dialogRef = this.dialogService.open(AddWidgetDialogComponent, {
        data: {
          title: 'Add Widget',
          titleBorder: true,
          dashboardName: this.route.snapshot.params['id']
        }
      });

      dialogRef.afterClosed.pipe(first()).subscribe(result => {
        this.router.navigate(['.']);
      });
    } else if (dashboardAction === 'visitor') {

      const dialogRef = this.dialogService.open(VisitorInfoDialogComponent, {
        data: {
          title: 'Visitor Info',
          titleBorder: true,
          visitor: this.route.snapshot.queryParamMap.get('visitor')
        }
      });

      dialogRef.afterClosed.pipe(first()).subscribe(result => {
        this.router.navigate(['.']);
      });
    }
  }

  private initVisitorDialog() {
    this.widgetDataService.getData('LastMatchWidgetViewIS', this).subscribe(response => {
      const visitor = get(response, 'response');
      if (!visitor || visitor.length <= 0) {
        return;
      }

      if (visitor.WatchListID === AppConstants.UserSettings.VisitorDialogWatchlist) {
        this.dialogService.open(WelcomeVisitorDialogComponent, {
          data: { visitor }
        });
      }
    });
  }

}
