/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DashboardSampleService } from './dashboard-sample.service';

describe('Service: DashboardSample', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DashboardSampleService]
    });
  });

  it('should ...', inject([DashboardSampleService], (service: DashboardSampleService) => {
    expect(service).toBeTruthy();
  }));
});
