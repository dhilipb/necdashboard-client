import { Injectable, OnDestroy } from '@angular/core';
import { ApiEntity, DashboardWidget } from '@core/model';
import { ApiService } from '@core/services';
import { get } from 'lodash';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DashboardSampleService implements OnDestroy {

  constructor(
    private apiService: ApiService
  ) { }

  ngOnDestroy() { }

  initSampleDashboard(dashboardName: string, sampleDashboard?: Array<DashboardWidget>) {
    console.log('Sample', 'Creating dashboard', dashboardName);
    return this.apiService.create(ApiEntity.DASHBOARD, { name: dashboardName })
      .pipe(map(result => {
        const dashboardId = get(result, 'response');
        if (!dashboardId) {
          console.error(result);
          throw new Error('Could not create dashboard');
        }

        // Create widgets
        return sampleDashboard.map(widget => {
          return this.createWidget(dashboardId, widget);
        });
      }));
  }

  private createWidget(dashboardId: string, widget: DashboardWidget) {
    console.log('Sample', 'Creating widget', dashboardId, widget);
    return this.apiService.create(ApiEntity.WIDGET, {
      type: widget.widgetId.split('_')[0],
      x: widget.x,
      y: widget.y,
      cols: widget.cols,
      rows: widget.rows,
      dashboardId
    }).pipe(map(result => {
      const widgetId = get(result, 'response');
      const options = widget.options || {};
      if (!widgetId) {
        console.error(result);
        throw new Error('Could not create widget');
      }

      return this.createWidgetSettings(options, widgetId);
    }));
  }


  private createWidgetSettings(options: any, widgetId: any) {
    console.log('Sample', 'Creating widget settings', options, widgetId);
    return Object.entries(options).map(([key, valueObj]) => {
      if (!valueObj['value']) {
        return;
      }
      return this.apiService.create(ApiEntity.WIDGET_SETTINGS, {
        key,
        widgetId,
        value: valueObj['value']
      });
    });
  }
}
