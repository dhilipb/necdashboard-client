import { Injectable, Injector } from '@angular/core';
import { ApiEntity } from '@core/model';
import { ApiService, LocalCacheService, StoreService } from '@core/services';
import { get } from 'lodash';
import { map } from 'rxjs/operators';

import { DashboardWidget } from '../../../core/model/dashboard-widget';
import { WidgetComponents } from '../../widget/widget-components';
import { AnalyticsSampleWidgets, HomeSampleWidgets } from '../dashboard-sample-widgets';
import { DashboardSampleService } from './dashboard-sample.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    private apiService: ApiService,
    private dashboardSampleService: DashboardSampleService,
    private storeService: StoreService,
    private localCacheService: LocalCacheService
  ) {

  }

  retrieveDashboards(injector?: Injector) {
    return this.apiService.request({
      entity: ApiEntity.DASHBOARD,
      action: 'all'
    })
      .pipe(
        map(result => {
          const dashboardsResponse = get(result, 'response', []);
          if (!dashboardsResponse.length) {
            if (confirm('No dashboards found. Would you like to create sample dashboards?')) {
              this.initSampleDashboards();
            }
          } else {
            console.log('Using API');
            this.transformDashboardData(dashboardsResponse);
          }
          if (injector) {
            this.updateInjectors(injector);
          }
          return dashboardsResponse;
        }));
  }

  private transformDashboardData(dashboardData: any) {
    const dashboards = {};

    dashboardData.forEach(row => {
      const dashboardName = row['name'];
      const widgetId = row['widgetId'];
      const widgetSettingsId = row['wid'];

      dashboards[dashboardName] = dashboards[dashboardName] || {};
      const widgetComponent = WidgetComponents[row['type']];
      if (widgetComponent) {
        dashboards[dashboardName][widgetId] = dashboards[dashboardName][widgetId] || {
          widgetId,
          component: widgetComponent,
          x: parseInt(row['x'], 10),
          y: parseInt(row['y'], 10),
          cols: parseInt(row['cols'], 10),
          rows: parseInt(row['rows'], 10),
          options: widgetComponent.prototype.getOptions() || {}
        };

        if (widgetSettingsId) {
          dashboards[dashboardName][widgetId]['options'][row['key']] = {
            widgetSettingsId,
            value: row['value']
          };
        }
      }

    });

    Object.keys(dashboards).forEach(key => {
      // Store in StoreService only if they are different
      const widgets: DashboardWidget[] = Object.values(dashboards[key]);
      if (JSON.stringify(this.storeService.dashboardItems[key]) !== JSON.stringify(widgets)) {
        this.storeService.dashboardItems[key] = widgets;
      }
    });
  }

  private initSampleDashboards() {
    this.storeService.dashboardItems = {
      home: HomeSampleWidgets,
      analytics: AnalyticsSampleWidgets
    };

    return Object.entries(this.storeService.dashboardItems).map(([name, widgets]) => {
      return this.dashboardSampleService.initSampleDashboard(name, widgets);
    });
  }

  private updateInjectors(injector: Injector) {
    Object.entries(this.storeService.dashboardItems).forEach(([name, widgets]) => {
      widgets.forEach(widget => {
        const widgetId = widget['widgetId'];
        this.storeService.widgetInjectors[widgetId] = this.storeService.widgetInjectors[widgetId] || Injector.create({
          providers: [{ provide: 'widgetId', useValue: widgetId }],
          parent: injector
        });
      });
    });
  }

  saveDashboard() {

  }

}
