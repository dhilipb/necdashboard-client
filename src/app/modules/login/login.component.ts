import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '@core/services/authentication.service';
import { untilDestroyed } from '@core/until-destroy';
import { DialogService } from '@shared/dialog';

import { LoginSettingsDialogComponent } from './login-settings-dialog/login-settings-dialog.component';

@Component({
  selector: 'dash-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  public loginForm: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });

  public isLoginValid: boolean = true;
  public loginLoading: boolean = false;

  constructor(private router: Router, private authService: AuthenticationService, private dialogService: DialogService) { }
  ngOnInit() {
    this.authService.isAuthenticated(true)
      .pipe(untilDestroyed(this))
      .subscribe(auth => {
        if (auth) {
          this.router.navigate(['']);
        }
      });
  }

  public login() {
    const username = this.loginForm.get('username').value;
    const password = this.loginForm.get('password').value;

    this.loginLoading = true;

    this.authService.login(username, password)
      .pipe(untilDestroyed(this))
      .subscribe(status => {
        if (status) {
          console.log('Login successful');
          this.router.navigate(['/dashboard']);
        } else {
          this.isLoginValid = false;
        }
        this.loginLoading = false;
      });
  }

  openSettings() {
    this.dialogService.open(LoginSettingsDialogComponent, {
      data: {
        title: 'Settings',
        titleBorder: true,
      }
    });
  }

  ngOnDestroy() { }
}
