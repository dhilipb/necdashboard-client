import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { DialogModule } from '../../shared/dialog/dialog.module';
import { SharedModule } from '../../shared/shared.module';
import { LoginSettingsDialogComponent } from './login-settings-dialog/login-settings-dialog.component';
import { LoginComponent } from './login.component';

const routes = [{ path: '', component: LoginComponent }];

@NgModule({
  declarations: [
    LoginComponent,
    LoginSettingsDialogComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    SharedModule,
    DialogModule
  ],
  exports: [
    RouterModule
  ],
  entryComponents: [
    LoginSettingsDialogComponent
  ]
})
export class LoginModule {
}
