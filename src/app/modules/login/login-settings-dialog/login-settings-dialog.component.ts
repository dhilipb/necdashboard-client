import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AppConstants, UserConfigService } from '@core/config';

@Component({
  selector: 'login-settings-dialog',
  templateUrl: './login-settings-dialog.component.html',
  styleUrls: ['./login-settings-dialog.component.scss']
})
export class LoginSettingsDialogComponent implements OnInit {

  public saveStatus: string = 'Save';
  public settingsForm: FormGroup = new FormGroup({});

  constructor(
    private userConfigService: UserConfigService,
  ) { }

  ngOnInit() {
    this.userConfigService.loadConfig();

    Object.entries(AppConstants.UserSettings).forEach(([key, value]) => {
      if (typeof value === 'boolean') {
        value = value ? 'true' : 'false';
      }
      this.settingsForm.registerControl(key, new FormControl(value));
    });
  }


  async onSubmit() {
    this.saveStatus = 'Saving..';
    await this.userConfigService.persistConfig(this.settingsForm);
    window.location.reload();
  }

}
