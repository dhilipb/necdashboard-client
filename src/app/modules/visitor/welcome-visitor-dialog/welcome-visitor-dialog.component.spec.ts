import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WelcomeVisitorDialogComponent } from './welcome-visitor-dialog.component';

/* tslint:disable:no-unused-variable */
describe('WelcomeVisitorDialogComponent', () => {
  let component: WelcomeVisitorDialogComponent;
  let fixture: ComponentFixture<WelcomeVisitorDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WelcomeVisitorDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomeVisitorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
