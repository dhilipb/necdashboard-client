import { Component, OnInit } from '@angular/core';
import { AppConstants } from '@core/config';
import { DialogConfig, DialogRef } from '@shared/dialog';
import { get } from 'lodash';

@Component({
  selector: 'dash-welcome-visitor-dialog',
  templateUrl: './welcome-visitor-dialog.component.html',
  styleUrls: ['./welcome-visitor-dialog.component.scss']
})
export class WelcomeVisitorDialogComponent implements OnInit {

  public visitor: any;

  constructor(
    private dialogConfig: DialogConfig,
    private dialogRef: DialogRef
  ) {
    this.visitor = get(this.dialogConfig, 'data.visitor');
  }

  ngOnInit() {
    setTimeout(() => {
      this.dialogRef.close();
    }, 3000);
  }

  getImage(user: any) {
    return AppConstants.UserSettings.NecUrl + '/Image/GetSubjectFaceThumbnail?id=' + user.SubjectPhotoId;
  }
  getCapturedImage(user: any) {
    return AppConstants.UserSettings.NecUrl + '/Image/GetFeaturePhoto?id=' + user.FeatureID;
  }

}
