import { Component, OnInit } from '@angular/core';
import { AppConstants } from '@core/config';
import { ApiEntity, NecVisitor } from '@core/model';
import { ApiService } from '@core/services';
import { DialogConfig, DialogRef } from '@shared/dialog';
import { get } from 'lodash';

import { WidgetDataService } from '../../widget/widgets/services/widget-data.service';

@Component({
  selector: 'dash-visitor',
  templateUrl: './visitor-info-dialog.component.html',
  styleUrls: ['./visitor-info-dialog.component.scss']
})
export class VisitorInfoDialogComponent implements OnInit {

  public visitor: any;
  public loading: boolean = false;

  constructor(
    private dialogConfig: DialogConfig,
    private dialogRef: DialogRef,
    private apiService: ApiService,
    private widgetDataService: WidgetDataService
  ) {
  }

  ngOnInit() {
    this.loading = true;
    const visitorId = get(this.dialogConfig, 'data.visitor');
    this.apiService.get(ApiEntity.VIEW, { viewName: 'LastMatchWidgetViewIS' }).subscribe(data => {
      const responses = get(data, 'response', []);
      this.loading = false;
      this.visitor = responses.find((v: NecVisitor) => v.SubjectId === visitorId);
      if (!this.visitor) {
        throw new Error('Could not find visitor');
      }
    });
  }

  getImage(user: any) {
    return AppConstants.UserSettings.NecUrl + '/Image/GetSubjectFaceThumbnail?id=' + user.SubjectPhotoId;
  }
  getCapturedImage(user: any) {
    return AppConstants.UserSettings.NecUrl + '/Image/GetFeaturePhoto?id=' + user.FeatureID;
  }
}
