import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';

import { VisitorInfoDialogComponent } from './visitor-info-dialog';
import { WelcomeVisitorDialogComponent } from './welcome-visitor-dialog';


@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    WelcomeVisitorDialogComponent,
    VisitorInfoDialogComponent
  ],
  exports: [
    WelcomeVisitorDialogComponent,
    VisitorInfoDialogComponent
  ],
  entryComponents: [
    WelcomeVisitorDialogComponent,
    VisitorInfoDialogComponent
  ]
})
export class VisitorModule { }
