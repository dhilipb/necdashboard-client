import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProfileComponent } from './profile.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{path: '', component: ProfileComponent}])
  ],
  declarations: [ProfileComponent]
})
export class ProfileModule { }
