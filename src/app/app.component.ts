import { Component, OnInit } from '@angular/core';

import { AuthGuardService } from './guards/auth-guard.service';

@Component({
  selector: 'dashboard-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    public authGuardService: AuthGuardService
  ) { }

  ngOnInit() {
  }
}
