import { Injectable, OnDestroy } from '@angular/core';
import { get } from 'lodash';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';

import { AppConstants } from '../config';
import { ApiRequest } from '../model';
import { ApiEntity } from '../model/api-entity';
import { ApiService } from './api.service';
import { LocalCacheService } from './local-cache.service';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService implements OnDestroy {

  constructor(
    private apiService: ApiService,
    private localCacheService: LocalCacheService
  ) {
  }

  public isAuthenticated(refresh: boolean = false) {
    if (!refresh && AppConstants.UserLoggedIn !== null) {
      return of(AppConstants.UserLoggedIn);
    }

    return this.apiService.request({
      entity: ApiEntity.USER,
      action: 'isAuthenticated'
    } as ApiRequest).pipe(map(result => {
      AppConstants.UserLoggedIn = get(result, 'response', false);
      return AppConstants.UserLoggedIn;
    }));
  }

  public login(username: string, password: string) {
    const expiryDate = AppConstants.UserSettings.ExpiryDate;
    const isExpired = new Date().getTime() > expiryDate;
    if (isExpired) {
      return of(false);
    }

    return this.apiService.request({
      entity: ApiEntity.USER,
      action: 'login',
      args: {
        username,
        password
      }
    } as ApiRequest).pipe(map(result => {
      AppConstants.UserLoggedIn = get(result, 'response', false);
      return AppConstants.UserLoggedIn;
    }));
  }

  public logout() {
    // remove user from local storage to log user out
    this.apiService.requestSingle({
      entity: ApiEntity.USER,
      action: 'logout'
    } as ApiRequest)
      .subscribe(() => {
        this.localCacheService.clear();
        window.location.reload();
      });
  }

  ngOnDestroy() { }
}
