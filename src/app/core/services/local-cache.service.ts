import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalCacheService {

  public store(key: string, item: any) {
    localStorage.setItem(key, JSON.stringify(item));
  }
  public remove(key: string) {
    localStorage.removeItem(key);
  }
  public get(key: string, defaultValue?: any) {
    return JSON.parse(localStorage.getItem(key)) || defaultValue;
  }
  public clear() {
    localStorage.clear();
  }
  public has(key: string): boolean {
    return localStorage.getItem(key) != null;
  }

}
