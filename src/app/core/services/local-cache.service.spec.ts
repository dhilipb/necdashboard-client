/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { LocalCacheService } from './local-cache.service';

describe('Service: LocalCache', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocalCacheService]
    });
  });

  it('should ...', inject([LocalCacheService], (service: LocalCacheService) => {
    expect(service).toBeTruthy();
  }));
});
