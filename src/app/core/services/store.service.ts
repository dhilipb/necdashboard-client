import { Injectable, Injector } from '@angular/core';
import { flatten } from 'lodash';

import { DashboardWidget } from '../model';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  public dashboardItems: { [key: string]: Array<DashboardWidget> } = {};
  public widgetInjectors: { [widgetId: string]: Injector } = {};

  constructor() { }

  getWidgets() {
    return flatten(Object.values(this.dashboardItems));
  }

  getWidget(widgetId: string) {
    const widgets = this.getWidgets();
    return widgets.find(w => w.widgetId === widgetId);
  }

}
