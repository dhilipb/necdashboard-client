import { Injectable } from '@angular/core';
import { ApiEntity } from '@core/model';

import { ApiRequest } from '../model';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class WatchlistService {

  constructor(
    private apiService: ApiService
  ) {

  }

  public getWatchlists() {
    return this.apiService.request({
      entity: ApiEntity.WATCHLIST
    } as ApiRequest);
  }

}
