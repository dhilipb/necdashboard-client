import { Injectable } from '@angular/core';

import { ApiRequest } from '../model';
import { ApiEntity } from '../model/api-entity';
import { ApiService } from './api.service';


@Injectable({
  providedIn: 'root'
})
export class CameraService {

  constructor(
    private apiService: ApiService
  ) {

  }

  public getCameras() {
    return this.apiService.request({
      entity: ApiEntity.CAMERAS
    } as ApiRequest);
  }

}
