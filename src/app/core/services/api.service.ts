import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { get } from 'lodash';
import { Subject } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { AppConstants } from '../config';
import { ApiResult } from '../model';
import { ApiEntity } from '../model/api-entity';
import { ApiRequest, ApiRequestWrapper } from '../model/api-request';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private timeoutId: any;

  private apiRequests: { [id: string]: ApiRequest } = {};
  private apiListeners: { [id: string]: Subject<any> } = {};

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  public request(requests: ApiRequest, combineRequests: boolean = true) {
    if (combineRequests) {
      return this.requestCombine(requests);
    } else {
      return this.requestSingle(requests);
    }
  }

  public requestCombine(apiRequest: ApiRequest) {
    const apiRequestId = Math.random().toString(36).substring(7);
    apiRequest.requestId = apiRequestId;
    this.apiRequests[apiRequestId] = apiRequest;

    const apiListener = new Subject<any>();
    this.apiListeners[apiRequestId] = apiListener;

    // Debounce and merge many API calls into one API call for optimisation
    clearTimeout(this.timeoutId);
    this.timeoutId = setTimeout(() => {
      const requestWrapper = { requests: Object.values(this.apiRequests) } as ApiRequestWrapper;
      this.apiRequests = {};

      this.http.post<ApiResult>(AppConstants.UserSettings.ApiUrl, requestWrapper, { withCredentials: true })
        .subscribe(results => {
          const requests = get(results, 'requests', []);
          for (let i = 0; i < requests.length; i++) {
            const request: any = get(results, `requests[${i}]`);
            const response: any = get(results, `response[${i}]`);
            const error: string = get(results, 'error');
            const status: boolean = get(results, 'status', true);

            if (!status && error === 'Unauthorized') {
              AppConstants.RealTimeEnabled = false;
              AppConstants.UserLoggedIn = false;
              this.router.navigate(['/login']);
            }

            if (status) {
              const requestId = request.requestId;
              const subject: Subject<any> = this.apiListeners[requestId];
              if (subject) {
                subject.next({ response });
              }

              delete this.apiListeners[requestId];
            } else {
              throw new Error(error);
            }
          }
        });
    }, 1000);

    return apiListener.pipe(first());
  }

  public requestSingle(requests: ApiRequest) {
    const requestWrapper = { requests: [requests] } as ApiRequestWrapper;
    return this.http.post<ApiResult>(AppConstants.UserSettings.ApiUrl, requestWrapper, { withCredentials: true })
      .pipe(map(result => {
        const response = get(result, 'response[0]');
        return { response };
      }));
  }

  public get(entity: ApiEntity, args?: any, combineRequests?: boolean) {
    return this.request({
      method: 'get',
      entity,
      args
    }, combineRequests);
  }
  public edit(entity: ApiEntity, args?: any, combineRequests?: boolean) {
    return this.request({
      method: 'edit',
      entity,
      args
    }, combineRequests);
  }
  public delete(entity: ApiEntity, args?: any, combineRequests?: boolean) {
    return this.request({
      method: 'delete',
      entity,
      args
    }, combineRequests);
  }
  public create(entity: ApiEntity, args?: any, combineRequests?: boolean) {
    return this.request({
      method: 'create',
      entity,
      args
    }, combineRequests);
  }

}
