import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppConfigDirective } from './config/app-config.directive';
import { AppConfigPipe } from './config/app-config.pipe';
import { DashboardHttpInterceptor } from './dash-http-interceptor';
import { DashboardLayoutComponent, FooterComponent, HeaderComponent, HeaderLayoutComponent, SimpleLayoutComponent } from './layout';
import { MemoizePipe } from './pipes';

const declarationsAndExports = [
  // Components
  HeaderComponent,
  FooterComponent,
  SimpleLayoutComponent,
  HeaderLayoutComponent,
  DashboardLayoutComponent,

  // Directives
  AppConfigDirective,

  // Pipes
  AppConfigPipe,
  MemoizePipe
];

@NgModule({
  declarations: declarationsAndExports,
  imports: [
    // SHOULD ONLY BE THIRD PARTY MODULES
    CommonModule,
    RouterModule,
    FontAwesomeModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: DashboardHttpInterceptor,
      multi: true
    }
  ],
  exports: declarationsAndExports
})
export class CoreModule { }
