import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { get } from 'lodash';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class DashboardHttpInterceptor implements HttpInterceptor {

    constructor(
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.body && request.body.success === false) {
            console.error(request.body);
        }
        return next.handle(request).pipe(
            tap(response => {
                if (response instanceof HttpResponse) {
                    if (response.status === 200) {
                        if (get(response, 'body.status') === false) {
                        }
                    }
                }
            })
        );
    }

}
