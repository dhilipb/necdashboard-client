export const AppConstants = {
    AppName: 'Command Center',
    RealTimeEnabled: true,
    UserLoggedIn: null,

    UserSettings: {
        // Generic
        NecUrl: 'http://192.168.0.55',
        ApiUrl: 'http://nec-dev.infitech.in/api/',
        RealTimeInterval: 2 * 1000,
        LongTimeInterval: 60 * 1000,
        ExpiryDate: 1584662400000,
        Locked: false,

        // Visitor dialog
        VisitorDialogWatchlist: 'VIP',
        ConfettiPieces: 30
    }
};
