import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { set } from 'lodash';

import { AppConstants } from './app-constants';

@Injectable({
  providedIn: 'root'
})
export class UserConfigService {
  private readonly STORAGE_KEY: string = 'userSettings';

  constructor() { }

  /**
   * Load from local storage
   */
  public loadConfig() {
    const userConfig = JSON.parse(localStorage.getItem(this.STORAGE_KEY) || '{}');
    for (const key of Object.keys(userConfig)) {
      let value: any = userConfig[key];
      if (value) {
        if (value === 'true' || value === 'false') {
          value = value === 'true';
        } else if (typeof value === 'string') {
          value = Date.parse(value) || value;
        }
        AppConstants.UserSettings[key] = value;
      }
    }
    console.log('Config', AppConstants.UserSettings);
  }

  public persistConfig(form?: FormGroup) {
    if (form) {

      Object.entries(AppConstants.UserSettings).forEach(([key, val]) => {
        let value = form.get(key).value;
        if (key === 'ExpiryDate') { value = new Date(value); }
        if (value) {
          set(AppConstants.UserSettings, key, value);
          console.log('Saving', key, value);
        }
      });

    }
    localStorage.setItem(this.STORAGE_KEY, JSON.stringify(AppConstants.UserSettings));
  }

}
