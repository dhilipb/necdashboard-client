/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { AppConfigDirective } from './app-config.directive';

describe('Directive: AppConfig', () => {
  it('should create an instance', () => {
    const directive = new AppConfigDirective();
    expect(directive).toBeTruthy();
  });
});
