import { Pipe, PipeTransform } from '@angular/core';

import { AppConstants } from './app-constants';

@Pipe({
  name: 'app-config'
})
export class AppConfigPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return AppConstants[value] || '';
  }

}
