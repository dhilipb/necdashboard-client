import { Directive, ElementRef, Input, OnInit } from '@angular/core';

import { AppConstants } from './app-constants';

@Directive({
  selector: '[appConfig]'
})
export class AppConfigDirective implements OnInit {

  @Input() private appConfig: string;

  constructor(
    private elementRef: ElementRef
  ) {

  }

  ngOnInit(): void {
    this.elementRef.nativeElement.innerHTML = AppConstants[this.appConfig] || ''
  }

}
