export enum ApiEntity {
    USER = 'user',
    DASHBOARD = 'dashboard',
    WIDGET = 'widget',
    WIDGET_SETTINGS = 'widgetSettings',
    USER_SETTINGS = 'userSettings',
    DB_VIEWS = 'db-views',
    CAMERAS = 'cameras',
    VIEW = 'view',
    COUNT = 'count',
    CHART_DATA = 'chart-data',
    PROCESS_STATS = 'process-stats',
    WATCHLIST = 'watchlist',
    LAST_MATCH_WIDGET_VIEW = 'LastMatchWidgetViewIS',
    WIDGET_DATA = 'widgetData'
}
