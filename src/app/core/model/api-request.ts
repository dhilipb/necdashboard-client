export interface ApiRequestWrapper {
    requests: Array<ApiRequest>;
}

export interface ApiRequest {
    requestId?: string;
    entity: string;
    method?: 'get' | 'edit' | 'delete' | 'create';
    action?: string;
    args?: any;
}
