export class Widget {
    constructor(
        public name: string,
        public columns: number,
        public type: string
    ) { }
}