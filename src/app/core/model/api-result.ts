export interface ApiResult {
  error?: string;
  response: Array<any>;
  status: boolean;
}