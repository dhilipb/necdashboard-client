export interface DashboardDto {
    created: string;
    enabled: string;
    id: string;
    locked: string;
    modified: string;
    name: string;
    userid: string;
}
