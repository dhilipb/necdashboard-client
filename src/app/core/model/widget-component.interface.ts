
export type AcceptedValue = string | boolean | number;

/**
 * Used to extend angular components which are widgets
 */
export interface WidgetComponent {
    getOptions(): WidgetComponentOptions;
}

export interface WidgetComponentOptions {
    widgetTitle: WidgetTitleOption | WidgetCustomOption;
    [customOptions: string]: WidgetTitleOption | WidgetCustomOption | WidgetSelectOption | WidgetListInputOption;
}

export interface WidgetTitleOption {
    value: string;
}


export interface BaseWidgetOption {
    title: string;
    subtitle?: string;
}

export interface WidgetCustomOption extends BaseWidgetOption {
    type?: 'text' | 'number' | 'db-views';
    value: AcceptedValue;
}

export interface WidgetListInputOption extends BaseWidgetOption {
    type: 'list-input' | 'maps';
    value: string;
}

export interface WidgetSelectOption extends BaseWidgetOption {
    type: 'select';
    value: string;
    options: Array<WidgetSelectOptionValue>;
}

export interface WidgetSelectOptionValue {
    title: string;
    value: string;
}
