export interface UserSetting {
    created: string;
    id: string;
    key: string;
    modified: string;
    userid: string;
    value: string;
}
