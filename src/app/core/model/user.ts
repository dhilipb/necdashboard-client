export interface User {
  id: number;
  name: string;
  avatar: string;
  email: string;
  lastVisit: string;
  alertType: string;
}
