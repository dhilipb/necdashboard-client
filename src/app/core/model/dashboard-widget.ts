import { Type } from '@angular/core';
import { GridsterItem } from 'angular-gridster2';

import { WidgetComponentOptions } from './widget-component.interface';

export interface DashboardWidget extends GridsterItem {
    widgetId: string;
    component: Type<any>;
    options?: WidgetComponentOptions;
}
