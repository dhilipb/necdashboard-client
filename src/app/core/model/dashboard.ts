import { Widget } from './widget';

export class Dashboard {
    constructor(
        public name: string,
        public widgets: Widget[]
    ) { }
}