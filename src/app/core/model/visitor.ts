export interface NecVisitor {
    MatchResultID: string;
    FirstName: string;
    MiddleName: string;
    LastName: string;
    CapDate: string;
    CapHour: string;
    LocationName: string;
    WatchlistName: string;
    SubjectId: string;
    SubjectPhotoId: string;
    CaptureID: string;
    Created: string;
    CameraID: string;
    SourceFilename: string;
    CameraName: string;
    LocationID: string;
    WatchListID: string;
    Title: string;
    FeatureID: string;
    Score: string;
    Colour: string;
    HideFromGUI: string;
    CameraEntryType: string;
    CaptureTime: string;
    DoorAddress: string;
    CustomField1: string;
    HexColour: string; // custom var
}
