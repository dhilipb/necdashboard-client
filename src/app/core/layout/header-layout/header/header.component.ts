import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ApiService, AuthenticationService, LocalCacheService } from '@core/services';
import { faCog, faExpandArrowsAlt, faPause, faPlus } from '@fortawesome/free-solid-svg-icons';
import { get } from 'lodash';
import { filter } from 'rxjs/operators';

import { AppConstants } from '../../../config/app-constants';
import { ApiEntity } from '../../../model/api-entity';
import { HeaderMenuItem, HeaderMenuItemFunction, HeaderMenuItemLink } from './header-menu-item';

@Component({
  selector: 'dash-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public headerMenuItems: Array<HeaderMenuItem> = [];
  public dashboardMenuItems: Array<HeaderMenuItemLink> = [];
  public dashboardContextMenuItems: Array<HeaderMenuItem> = [];
  public isDashboardContextMenuItemsEnabled: boolean = false;

  public get isFullScreen(): boolean {
    return document['webkitIsFullScreen'] || document['mozFullScreen'] || false;
  }

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private apiService: ApiService,
    private authenticationService: AuthenticationService,
    private localCacheService: LocalCacheService
  ) {
    this.initHeaderMenuItems();
    this.initDashboardContextMenuItems();
  }


  ngOnInit() {
    this.initDashboardMenuItems();
  }

  private initDashboardContextMenuItems() {
    this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(event => {
      this.dashboardContextMenuItems = [];
      if (event['urlAfterRedirects'].includes('/dashboard')) {
        this.dashboardContextMenuItems = [
          {
            title: 'Add Widget',
            type: 'link',
            icon: faPlus,
            link: '.',
            queryParams: { action: 'add' },
            enabled: true
          } as HeaderMenuItemLink,
          {
            title: 'Realtime',
            type: 'function',
            icon: faPause,
            method: () => { this.toggleRealTime(); },
            active: () => !AppConstants.RealTimeEnabled,
            enabled: true
          } as HeaderMenuItemFunction,
        ];
      }
    });
  }

  private initHeaderMenuItems() {
    this.headerMenuItems = [
      {
        title: 'Settings',
        type: 'link',
        icon: faCog,
        link: '/settings',
        enabled: !AppConstants.UserSettings.Locked
      } as HeaderMenuItemLink,
      {
        title: 'Fullscreen',
        type: 'function',
        icon: faExpandArrowsAlt,
        method: () => { this.toggleFullScreen(); },
        active: () => this.isFullScreen,
        enabled: true
      } as HeaderMenuItemFunction,
      {
        title: 'Logout',
        type: 'function',
        enabled: true,
        method: () => { this.logout(); }
      } as HeaderMenuItemFunction
    ];
  }
  private initDashboardMenuItems() {
    this.dashboardMenuItems = this.localCacheService.get('dashboardMenuItems', []);

    this.apiService.get(ApiEntity.DASHBOARD).subscribe(result => {
      const dashboards = get(result, 'response', []);
      this.dashboardMenuItems = dashboards.map(dashboard => ({
        title: dashboard.name,
        type: 'link',
        link: '/dashboard/' + dashboard.name.toLowerCase(),
        enabled: dashboard.enabled === '1' || false
      } as HeaderMenuItemLink));

      this.localCacheService.store('dashboard', dashboards);
      this.localCacheService.store('dashboardMenuItems', this.dashboardMenuItems);
    });
  }

  public toggleFullScreen() {
    const element = document.body;

    element['requestFullScreen'] = element['requestFullScreen'] || element['webkitRequestFullScreen'] || element['mozRequestFullScreen'] || (() => false);
    document['cancelFullScreen'] = document['cancelFullScreen'] || document['webkitCancelFullScreen'] || document['mozCancelFullScreen'] || (() => false);

    console.log(this.isFullScreen);
    this.isFullScreen ? document['cancelFullScreen']() : element['requestFullScreen']();
  }

  public toggleRealTime() {
    AppConstants.UserSettings.RealTimeInterval = AppConstants.UserSettings.RealTimeInterval === Number.MAX_VALUE ? 5000 : Number.MAX_VALUE;
    AppConstants.RealTimeEnabled = !AppConstants.RealTimeEnabled;
  }

  public logout() {
    this.authenticationService.logout();
  }


}
