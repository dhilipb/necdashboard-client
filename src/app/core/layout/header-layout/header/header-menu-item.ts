import { IconDefinition } from '@fortawesome/fontawesome-svg-core';

interface BaseHeaderMenuItem {
  title: string;
  icon?: IconDefinition;
  enabled: boolean;
}

export type HeaderMenuItem = HeaderMenuItemLink | HeaderMenuItemFunction;

export interface HeaderMenuItemLink extends BaseHeaderMenuItem {
  link: string;
  type: 'link';
  queryParams?: Object;
}

export interface HeaderMenuItemFunction extends BaseHeaderMenuItem {
  type: 'function';
  method: () => void;
  active?: () => boolean;
}
