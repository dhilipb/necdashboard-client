import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ConfettiComponent } from './confetti.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ConfettiComponent],
  exports: [ConfettiComponent]
})
export class ConfettiModule { }
