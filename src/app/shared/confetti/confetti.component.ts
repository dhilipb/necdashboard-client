import { Component, OnInit } from '@angular/core';
import { AppConstants } from '@core/config';

@Component({
  selector: 'confetti',
  templateUrl: './confetti.component.html',
  styleUrls: ['./confetti.component.scss']
})
export class ConfettiComponent implements OnInit {

  public numberOfPieces: number = AppConstants.UserSettings.ConfettiPieces;

  constructor() { }

  ngOnInit() {
  }

}
