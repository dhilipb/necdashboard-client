import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class VisitorUtil {

    isSameVisitor(visitor1, visitor2) {
        return visitor1 && visitor2 &&
            visitor1.CameraID == visitor2.CameraID &&
            visitor1.FeatureID == visitor2.FeatureID &&
            visitor1.CaptureID == visitor2.CaptureID;
    }

}