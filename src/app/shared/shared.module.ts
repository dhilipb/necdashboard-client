import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FormInputComponent } from './form-input/form-input.component';
import { LoadingComponent, LoadingDirective } from './loading';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    LoadingDirective,
    LoadingComponent,
    FormInputComponent
  ],
  exports: [
    LoadingDirective,
    LoadingComponent,
    FormInputComponent
  ],
  entryComponents: [
    LoadingComponent
  ]
})
export class SharedModule { }
