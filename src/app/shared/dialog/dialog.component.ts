import { AfterViewInit, ChangeDetectorRef, Component, ComponentFactoryResolver, ComponentRef, HostListener, OnDestroy, Type, ViewChild } from '@angular/core';
import { get } from 'lodash';
import { Observable, Subject } from 'rxjs';

import { DialogConfig } from './dialog-config';
import { DialogRef } from './dialog-ref';
import { InsertionDirective } from './insertion/insertion.directive';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements AfterViewInit, OnDestroy {

  private readonly onClose$: Subject<any> = new Subject<any>();

  public componentRef: ComponentRef<any>;
  public childComponentType: Type<any>;
  public childComponentOptions: any;
  public onClose: Observable<any> = this.onClose$.asObservable();
  public dialogTitle: string;
  public hasTitleBorder: boolean = false;

  @ViewChild(InsertionDirective, { static: false }) insertionPoint: InsertionDirective;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private cd: ChangeDetectorRef,
    private dialogRef: DialogRef,
    dialogConfig: DialogConfig
  ) {
    this.hasTitleBorder = get(dialogConfig, 'data.titleBorder') === true;
    const dialogTitle = get(dialogConfig, 'data.title');
    if (dialogTitle) {
      this.dialogTitle = dialogTitle;
    }
  }

  ngAfterViewInit() {
    this.loadChildComponent(this.childComponentType);
    this.cd.detectChanges();
  }

  ngOnDestroy() {
    if (this.componentRef) {
      this.componentRef.destroy();
    }
  }

  loadChildComponent(componentType: Type<any>) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(componentType);

    const viewContainerRef = this.insertionPoint.viewContainerRef;
    viewContainerRef.clear();

    this.componentRef = viewContainerRef.createComponent(componentFactory);
  }

  @HostListener('document:keydown.escape', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    this.dialogRef.close();
  }

  onOverlayClicked(event: MouseEvent) {
    // this.dialogRef.close();
  }
  onCloseClicked(event: MouseEvent) {
    this.dialogRef.close();
  }
  onContentClicked(event: MouseEvent) {
    event.stopPropagation();
  }

}
