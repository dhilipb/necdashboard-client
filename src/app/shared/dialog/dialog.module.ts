import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DialogComponent } from './dialog.component';
import { InsertionDirective } from './insertion/insertion.directive';
import { DialogService } from './services';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    DialogService
  ],
  declarations: [
    DialogComponent,
    InsertionDirective
  ],
  entryComponents: [
    DialogComponent
  ]
})
export class DialogModule { }
