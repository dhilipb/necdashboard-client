import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppConstants, UserConfigService } from '@core/config';
import { GridsterModule } from 'angular-gridster2';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    GridsterModule,
    HttpClientModule,
    CoreModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    userConfigService: UserConfigService
  ) {
    userConfigService.loadConfig();

    // Logout if expired
    const expiryDate = AppConstants.UserSettings.ExpiryDate;
    const isExpired = new Date().getTime() > expiryDate;
    if (isExpired) {
      localStorage.clear();
    }

    // Internal version number to delete location storage if a new version is pushed out
    const currentVersion = '2502';
    const version = localStorage.getItem('version');
    if (!version || version !== currentVersion) {
      if (localStorage.length > 2) {
        localStorage.clear();
      }
      localStorage.setItem('version', currentVersion);
      if (!window.location.hash.includes('login')) {
        window.location.reload();
      }
    }

  }
}
