import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '@core/services';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  public loading: boolean = false;

  constructor(private authService: AuthenticationService, private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.loading = true;
    return this.authService.isAuthenticated().pipe(map(auth => {
      if (!auth) {
        this.router.navigate(['/login']);
      }
      this.loading = false;
      return auth;
    }));
  }
}
