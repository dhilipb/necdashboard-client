# base image
FROM node:12.2.0 as build

# set working directory
WORKDIR /app

RUN echo 'set mouse=a' > ~/.vimrc

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json /app/package.json
RUN npm install
RUN npm install -g @angular/cli@7.3.9

# add app
COPY . /app

# generate build
RUN ng build --output-path=dist

############
### php ###
############

# base image
FROM gjuniioor/php-sqlsrv:7.0

RUN apt update && apt install vim -qq -y

# copy artifact build from the 'build environment'
COPY --from=build /app/dist /var/www/html/

WORKDIR /var/www/html/

EXPOSE 80
